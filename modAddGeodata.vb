Option Strict Off
Option Explicit On
Module modAddGeodata

   Public Const GEO_OFF_N_CHECK As Integer = 232
   Public m_pl As MapObjects2.PlaceLocator
   Public m_gcs As MapObjects2.GeoCoordSys
   Public m_pcs As MapObjects2.ProjCoordSys
   Public m_places As New Collection

   Dim pts As MapObjects2.Points

   'Function return 0 if not found.  sLong and sLat will be set to blank
   Public Function getGeoPointF(ByRef sFmtApn As String, ByRef sLong As String, ByRef sLat As String) As Short
      Dim ptInput As MapObjects2.Point
      Dim ptUnproj As MapObjects2.Point
      'Dim myPts As ESRI.MapObjects2.Core.Points

      'attempt an exact match of the APN
      pts = m_pl.Locate(sFmtApn)
      If pts.Count > 0 Then
         ptInput = pts.Item(0)
         ptUnproj = m_gcs.Transform(m_pcs, ptInput)
         sLong = VB6.Format(ptUnproj.X, "#0.00000000")
         sLat = VB6.Format(ptUnproj.Y, "#0.00000000")
         'For Each ptInput In pts
         '   ptUnproj = m_gcs.Transform(m_pcs, ptInput)

         '   'Display the projected and unprojected mouse coordinates.
         '   sLong = VB6.Format(ptUnproj.X, "#0.00000000")
         '   sLat = VB6.Format(ptUnproj.Y, "#0.00000000")
         '   Exit For
         'Next ptInput
         getGeoPointF = 1
      Else
         sLong = Space(13)
         sLat = Space(11)
         getGeoPointF = 0
      End If
   End Function

   'Function return 0 if not found.  sLong and sLat will be set to blank
   Public Function getGeoPointL(ByRef sFmtApn As String, ByRef sLong As String, ByRef sLat As String) As Short
      Dim ptInput As MapObjects2.Point
      Dim ptUnproj As MapObjects2.Point
      Dim moStr As New MapObjects2.Strings

      'attempt an exact match of the APN
      pts = m_pl.Locate(sFmtApn)
      If pts.Count > 0 Then
         ptInput = pts.Item(0)
         If g_bProjected Then
            ptUnproj = ptInput
         Else
            ptUnproj = m_gcs.Transform(m_pcs, ptInput)
         End If

         'Display the projected and unprojected mouse coordinates.
         sLong = VB6.Format(ptUnproj.X, "#0.000000")
         sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")

         'For Each ptInput In pts
         '   'Fix FRE bug - If map has been projected, no need to transform it again
         '   If g_bProjected Then
         '      ptUnproj = ptInput
         '   Else
         '      ptUnproj = m_gcs.Transform(m_pcs, ptInput)
         '   End If
         '   'Set ptUnproj = m_gcs.Transform(m_pcs, ptInput)

         '   'Display the projected and unprojected mouse coordinates.
         '   sLong = VB6.Format(ptUnproj.X, "#0.000000")
         '   sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")
         '   Exit For
         'Next ptInput
         getGeoPointL = 1
      Else

         moStr = m_pl.FindAllPlaceNames(sFmtApn)

         If moStr.Count > 0 Then
            m_places = Nothing
            pts = m_pl.Locate(moStr._Item(0))
            ptInput = pts.Item(0)
            If g_bProjected Then
               ptUnproj = ptInput
            Else
               ptUnproj = m_gcs.Transform(m_pcs, ptInput)
            End If

            'Display the projected and unprojected mouse coordinates.
            sLong = VB6.Format(ptUnproj.X, "#0.000000")
            sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")

            'For Each ptInput In pts
            '   'Fix FRE bug - If map has been projected, no need to transform it again
            '   If g_bProjected Then
            '      ptUnproj = ptInput
            '   Else
            '      ptUnproj = m_gcs.Transform(m_pcs, ptInput)
            '   End If
            '   'Set ptUnproj = m_gcs.Transform(m_pcs, ptInput)

            '   'Display the projected and unprojected mouse coordinates.
            '   sLong = VB6.Format(ptUnproj.X, "#0.000000")
            '   sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")
            '   Exit For
            'Next ptInput

            getGeoPointL = 1
         Else
            sLong = Space(11)
            sLat = Space(10)
            getGeoPointL = 0
         End If
      End If
   End Function

   'Function return 0 if not found.  sLong and sLat will be set to blank
   Public Function getGeoRec(ByRef sFmtApn As String, ByRef sLong As String, ByRef sLat As String, ByRef sOutBuf As String) As Short
      Dim ptInput As MapObjects2.Point
      Dim ptUnproj As MapObjects2.Point
      Dim moStr As New MapObjects2.Strings
      Dim sTmp As String

      'Init
      sTmp = sOutBuf

      'attempt an exact match of the APN
      pts = m_pl.Locate(sFmtApn)

      If pts.Count > 0 Then
         ptInput = pts.Item(0)
         ptUnproj = m_gcs.Transform(m_pcs, ptInput)

         'Display the projected and unprojected mouse coordinates.
         sLong = VB6.Format(ptUnproj.X, "#0.000000")
         sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")

         'For Each ptInput In pts
         '   ptUnproj = m_gcs.Transform(m_pcs, ptInput)

         '   'Display the projected and unprojected mouse coordinates.
         '   sLong = VB6.Format(ptUnproj.X, "#0.000000")
         '   sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")
         '   Exit For
         'Next ptInput
         getGeoRec = 1
      Else

         moStr = m_pl.FindAllPlaceNames(sFmtApn)

         If moStr.Count > 0 Then
            m_places = Nothing
            pts = m_pl.Locate(moStr._Item(0))
            ptInput = pts.Item(0)
            ptUnproj = m_gcs.Transform(m_pcs, ptInput)

            'Display the projected and unprojected mouse coordinates.
            sLong = VB6.Format(ptUnproj.X, "#0.000000")
            sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")

            'For Each ptInput In pts
            '   ptUnproj = m_gcs.Transform(m_pcs, ptInput)

            '   'Display the projected and unprojected mouse coordinates.
            '   sLong = VB6.Format(ptUnproj.X, "#0.000000")
            '   sLat = VB6.Format(ptUnproj.Y, "#0.000000 ")
            '   Exit For
            'Next ptInput

            getGeoRec = 1
         Else
            sLong = Space(11)
            sLat = Space(10)
            getGeoRec = 0
         End If
      End If
   End Function

   'This function replaces long/lat only.  Output same record length
   Public Sub geoBatch()
      Dim sBuf1, sBufOut As String
      Dim iTmp, iRet As Short
      Dim lNoPoint, lRet1, lCnt, lOldPoint As Integer
      Dim aBytes() As Byte
      Dim sLong, sApn, sLat As String

      Dim fcInfile As New clsBigFile
      'Dim fcOutput As New clsBigFile
      Dim fhOut As Integer

      On Error GoTo geoBatch_Error

      'Signal program is running
      g_bStop = False

      'Open input data file
      LogMsg("Open input file: " & g_srcFile)
      fcInfile.OpenFile(g_srcFile)

      'Open output data file
      LogMsg("Open output file: " & g_dstFile)
      If Dir(g_dstFile) <> "" Then
         Kill(g_dstFile)
      End If
      'fcOutput.OpenFile(g_dstFile)
      fhOut = FreeFile()
      FileOpen(fhOut, g_dstFile, OpenMode.Output, OpenAccess.Default)

      'Read first row of data
      lRet1 = fcInfile.ReadRecs(g_iRecSize, sBuf1)
      If Left(sBuf1, 6) = "999999" Then
         'Skip first record
         LogMsg("Skip first record of file 1")
         lRet1 = fcInfile.ReadRecs(g_iRecSize, sBuf1)
      End If

      lCnt = 1
      lNoPoint = 0
      lOldPoint = 0
      Do While lRet1 = g_iRecSize
         Select Case g_iApnFormat
            Case 1
               sApn = formatApn(Left(sBuf1, g_iApnLen))
            Case 2
               'SFX case
               sApn = removeBlankAt(Left(sBuf1, g_iApnLen), 5)
               'sApn = Left(sBuf1, g_iApnLen)
               'If Mid(sApn, 5, 1) = " " Then
               '   sApn = removeBlankAt(sApn, 5)
               'End If
            Case 3
               'CAL case
               sApn = removeLeadingZero(Left(sBuf1, g_iApnLen))
            Case Else
               sApn = Left(sBuf1, g_iApnLen)
         End Select

         'Locate APN
         If g_iUseGis > 0 Then
            'This function has not been tested - Sony
            iRet = getGeoRec(sApn, sLong, sLat, sBuf1)
         Else
            iRet = getGeoPointL(sApn, sLong, sLat)

            If g_bReplaceOnly Then
               'Here we replace long/lat only if found
               If iRet > 0 Then
                  iTmp = g_iGeoPos - 1
                  sBuf1 = Left(sBuf1, iTmp) & sLong & sLat & Right(sBuf1, g_iRecSize - (iTmp + 21))

                  'Set Gis flag = Y
                  sBuf1 = Left(sBuf1, GEO_OFF_N_CHECK) & "Y" & Right(sBuf1, g_iRecSize - (GEO_OFF_N_CHECK + 1))
               Else
                  sBuf1 = Left(sBuf1, GEO_OFF_N_CHECK) & " " & Right(sBuf1, g_iRecSize - (GEO_OFF_N_CHECK + 1))
               End If
            Else
               'Here we have to append whether long/lat is found or not
               'Output record - we need to add space for CR, Census Tract, and Deliver point
               'sBuf1 = sBuf1 & "    " & sLong & sLat & "               "
               sBuf1 = sBuf1 & "    " & sLong & sLat & "                 "
            End If
         End If

         If iRet = 0 Then
            lNoPoint = lNoPoint + 1
            'Check to see if existing long/lat there
            If Mid(sBuf1, g_iGeoPos, 4) > "    " Then
               lOldPoint = lOldPoint + 1
               If g_bDebug Then
                  LogMsg("APN not found: " & sApn & "->" & Mid(sBuf1, g_iGeoPos, 20))
               End If
            Else
               If g_bDebug Then
                  LogMsg("APN not found: " & sApn)
               End If
            End If
         End If

         'Convert from Unicode to ANSI for writing
         'aBytes = System.Text.UnicodeEncoding.Unicode.GetBytes(sBuf1)
         'fcOutput.WriteBytes(aBytes)
         sBufOut = Left(sBuf1, g_iRecSize - 2)
         PrintLine(fhOut, sBufOut)

         'Read next
         lRet1 = fcInfile.ReadRecs(g_iRecSize, sBuf1)
         lCnt = lCnt + 1

         If (lCnt Mod 1000) = 0 Then
            '         If g_bStop Then
            '            If MsgBox("Are you sure you want to stop?", vbYesNo, "Stop geocoding") = vbYes Then
            '               LogMsg "Stopped by user request at " & lCnt
            '               Exit Do
            '            End If
            '         End If

            System.Windows.Forms.Application.DoEvents()
         End If
      Loop

      'Flush output
      'fcOutput.Flush()

      'Close files
      fcInfile.CloseFile()
      'fcOutput.CloseFile()
      FileClose(fhOut)

      LogMsg("Number of records processed       : " & lCnt)
      LogMsg("Number of recs geocoded /w GIS    : " & lCnt - lNoPoint)
      LogMsg("Number of recs geocoded /w Centrus: " & lOldPoint)
      lRet1 = lCnt - lNoPoint + lOldPoint
      LogMsg("Total number of recs geocoded     : " & lRet1)
      LogMsg("Percentage of geocoded records    : " & VB6.Format((lRet1 / lCnt) * 100, "##.##") & "%")

      GoTo geoBatch_Exit

geoBatch_Error:
      LogMsg("***** Error Geocoding data: " & Err.Description)

geoBatch_Exit:
      g_bStop = True
      'fcOutput = Nothing
      fcInfile = Nothing
   End Sub

   Public Function initEsri() As Short
      Dim layer As New MapObjects2.MapLayer
      Dim dc As New MapObjects2.DataConnection
      Dim pGd As MapObjects2.GeoDataset

      On Error GoTo initEsri_Error

      'Just to make sure
      m_pl = Nothing
      m_gcs = Nothing
      m_pcs = Nothing

      m_gcs = New MapObjects2.GeoCoordSys
      m_pcs = New MapObjects2.ProjCoordSys
      m_pl = New MapObjects2.PlaceLocator

      dc.Database = g_sGISBase
      dc.Connect()

      pGd = dc.FindGeoDataset(g_sGISTable)
      If pGd Is Nothing Then
         LogMsg("***** Cannot find Geo Dataset " & g_sGISTable)
         GoTo initEsri_Error
      End If

      m_pl.PlaceNameTable = pGd

      'Build index for search field - do not build if index already exist
      If Not m_pl.BuildIndex(g_sGISIndex, False) Then
         LogMsg("***** Couldn't build index")
         GoTo initEsri_Error
      End If

      'Setup unprojected coordinate system
      m_gcs.Type = g_iGeoCS

      'Setup projected coordinate system
      m_pcs.Type = g_iPrjCS

      'Setup layer
      layer.GeoDataset = pGd

      'Tell layer what coordinate system to load - projected
      layer.CoordinateSystem = m_pcs
      layer.Symbol.Color = System.Convert.ToUInt32(MapObjects2.ColorConstants.moPaleYellow)

      initEsri = 0
      GoTo initEsri_Exit

initEsri_Error:
      initEsri = -1
      m_pl = Nothing
      m_gcs = Nothing
      m_pcs = Nothing

initEsri_Exit:
      dc = Nothing
      layer = Nothing
   End Function

   Public Sub doCleanup()
      m_pl = Nothing
      m_gcs = Nothing
      m_pcs = Nothing
   End Sub

   Public Sub doGeocode()
      Dim iRet As Integer

      iRet = initEsri()
      If iRet = 0 Then
         LogMsg("Geocoding " & g_sCounty)
         Call geoBatch()
      Else
         LogMsg("***** Fail initializing MapObjects.  Please contact Sony")
      End If
   End Sub

   Public Sub doGeoInit()
      Dim iRet As Integer

      iRet = initEsri()
      If iRet = 0 Then
         LogMsg("Initialize " & g_sCounty)
      Else
         LogMsg("***** Fail initializing MapObjects.  Please contact Sony")
      End If
   End Sub
End Module