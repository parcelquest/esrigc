Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Friend Class frmLocator
   Inherits System.Windows.Forms.Form

   Private Declare Sub Geo2Long Lib "prodlib" (ByVal dVal As String, ByVal lVal As String)

   Private Sub cmdBatch_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdBatch.Click
      'Disable buttons
      cmdLocateApn.Enabled = False
      cmdBatch.Enabled = False

      Call geoBatch()

      cmdLocateApn.Enabled = True
      cmdBatch.Enabled = True

   End Sub

   Private Sub cmdConnect_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdConnect.Click
      Dim layer As New MapObjects2.MapLayer
      Dim dc As New MapObjects2.DataConnection
      Dim pGd As MapObjects2.GeoDataset

      On Error GoTo cmdConnect_Error

      'Clean up existing image
      Call termGis()
      m_gcs = New MapObjects2.GeoCoordSys
      m_pcs = New MapObjects2.ProjCoordSys
      m_pl = New MapObjects2.PlaceLocator

      If g_sCounty <> txtCounty.Text Then
         g_sCounty = txtCounty.Text
         If Not initCounty(g_sCounty) Then
            MsgBox("Error initializing county: " & g_sCounty)
            Exit Sub
         End If

         txtGISBase.Text = g_sGISBase
         txtGISTable.Text = g_sGISTable
         txtGISIndex.Text = g_sGISIndex
         txtGeoCS.Text = CStr(g_iGeoCS)
         txtPrjCS.Text = CStr(g_iPrjCS)
      End If

      'Update variable
      g_sGISBase = txtGISBase.Text
      g_sGISTable = txtGISTable.Text
      g_sGISIndex = txtGISIndex.Text
      g_iGeoCS = CInt(txtGeoCS.Text)
      g_iPrjCS = CInt(txtPrjCS.Text)

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
      dc.Database = g_sGISBase
      dc.Connect()

      pGd = dc.FindGeoDataset(g_sGISTable)
      If pGd Is Nothing Then
         MsgBox("Cannot find Geo Dataset " & g_sGISTable)
         GoTo cmdConnect_Exit
      End If

      m_pl.PlaceNameTable = pGd

      'Build index for search field - do not build if index already exist
      If Not m_pl.BuildIndex(g_sGISIndex, False) Then
         MsgBox("Couldn't build index")
      End If

      'Setup unprojected coordinate system
      'CCX, SAC, EDX, AMA, YOL, SON are Nad1983
      m_gcs.Type = g_iGeoCS

      'Setup projected coordinate system
      m_pcs.Type = g_iPrjCS

      'Setup layer
      layer.GeoDataset = pGd

      'Tell layer what coordinate system to load - projected
      layer.CoordinateSystem = m_pcs

      Map1.Layers.Add(layer)
      layer.Symbol.Color = System.Convert.ToUInt32(MapObjects2.ColorConstants.moPaleYellow)

      VB6.SetDefault(cmdConnect, False)
      cmdConnect.Enabled = False
      cmdLocateApn.Enabled = True
      VB6.SetDefault(cmdLocateApn, True)
      cmdBatch.Enabled = True
      g_bLoaded = True

      GoTo cmdConnect_Exit

cmdConnect_Error:
      MsgBox("Error connecting to GIS system: " & Err.Description)

cmdConnect_Exit:
      'Clean up
      dc = Nothing
      layer = Nothing
      Me.Cursor = System.Windows.Forms.Cursors.Default
   End Sub

   Private Sub cmdExit_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdExit.Click
      Me.Close()
   End Sub

   Private Sub cmdLocateApn_Click(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles cmdLocateApn.Click
      Dim pts As MapObjects2.Points
      Dim p As MapObjects2.Point
      Dim strs As New MapObjects2.Strings
      Dim i, j As Integer
      Dim sTmp As String
      Dim m1_places As New Collection

      '08/22/2014
      'm_places = Nothing

      If g_iApnFormat = 1 Then
         sTmp = formatApn((txtApn.Text))
      ElseIf g_iApnFormat = 2 Then
         sTmp = removeBlank((txtApn.Text))
      Else
         sTmp = VB.Left(txtApn.Text, g_iApnLen)
      End If

      If sTmp = "" Then
         MsgBox("Invalid APN: " & txtApn.Text)
         Exit Sub
      End If

      'attempt an exact match of the place name
      pts = m_pl.Locate(sTmp)

      If pts.Count > 0 Then
         'For Each p In pts
         '   m_places.Add(p)
         'Next p
         For j = 0 To pts.Count - 1
            p = pts.Item(j)
            m1_places.Add(p)
         Next j
         strs.Add(sTmp)
         Call showCoordinate(m1_places)
      Else
         strs = m_pl.FindAllPlaceNames(txtApn.Text)

         If strs.Count > 0 Then
            'm_places = Nothing
            For i = 0 To strs.Count - 1
               pts = m_pl.Locate(strs._Item(i))
               'For Each p In pts
               '   m_places.Add(p)
               'Next p
               For j = 1 To pts.Count
                  m1_places.Add(pts.Item(j))
               Next j
            Next i
            Call showCoordinate(m1_places)
         Else
            MsgBox("No matching records found")
         End If
      End If

      'populate the list with place names
      List1.Items.Clear()
      Dim s As String
      For i = 0 To strs.Count - 1
         s = strs.Item(i)
         List1.Items.Add(s)
      Next
      'For Each s In strs
      '   List1.Items.Add(s)
      'Next s
      txtCounts.Text = CStr(List1.Items.Count)

      'draw place name locations
      Map1.TrackingLayer.Refresh(True)
   End Sub

   Private Sub frmLocator_Load(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles MyBase.Load
      Me.Text = Me.Text & " " & My.Application.Info.Version.Major & "." & My.Application.Info.Version.Minor & "." & My.Application.Info.Version.Revision
      txtGISBase.Text = g_sGISBase
      txtGISTable.Text = g_sGISTable
      txtGISIndex.Text = g_sGISIndex
      txtGeoCS.Text = CStr(g_iGeoCS)
      txtPrjCS.Text = CStr(g_iPrjCS)
      txtCounty.Text = g_sCounty
   End Sub

   Private Sub frmLocator_FormClosed(ByVal eventSender As System.Object, ByVal eventArgs As System.Windows.Forms.FormClosedEventArgs) Handles Me.FormClosed
      Call termGis()
   End Sub

   Private Sub termGis()
      m_places = Nothing
      m_pl = Nothing
      m_gcs = Nothing
      m_pcs = Nothing
      Map1.Layers.Clear()
   End Sub

   Private Sub Map1_AfterTrackingLayerDraw(ByVal eventSender As System.Object, ByVal eventArgs As AxMapObjects2._DMapEvents_AfterTrackingLayerDrawEvent) Handles Map1.AfterTrackingLayerDraw
      If Not m_places Is Nothing Then
         Call DrawPlaces(m_places)
      End If
   End Sub

   Sub DrawPlaces(ByRef places As Collection)
      Dim sym As New MapObjects2.Symbol
      Dim p As MapObjects2.Point
      sym.SymbolType = MapObjects2.SymbolTypeConstants.moPointSymbol
      sym.Style = MapObjects2.MarkerStyleConstants.moCircleMarker
      sym.Color = System.Convert.ToUInt32(MapObjects2.ColorConstants.moRed)
      sym.size = 7

      For Each p In places
         Map1.DrawShape(p, sym)
      Next p
   End Sub

   Private Sub Map1_MouseDownEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxMapObjects2._DMapEvents_MouseDownEvent) Handles Map1.MouseDownEvent
      If eventArgs.Shift = VB6.ShiftConstants.ShiftMask Then
         Map1.Extent = Map1.FullExtent
      Else
         Map1.Extent = Map1.TrackRectangle
      End If
   End Sub

   Private Sub Map1_MouseMoveEvent(ByVal eventSender As System.Object, ByVal eventArgs As AxMapObjects2._DMapEvents_MouseMoveEvent) Handles Map1.MouseMoveEvent
      Dim ptProj As MapObjects2.Point
      Dim ptUnproj As MapObjects2.Point
      Dim ptInput As MapObjects2.Point
      ptInput = Map1.ToMapPoint(eventArgs.x, eventArgs.y)

      If Not g_bLoaded Then
         Exit Sub
      End If

      'Project or unproject the mouse location
      If Map1.Extent.Right > 500 Then

         ptProj = ptInput
         ptUnproj = m_gcs.Transform(m_pcs, ptProj)
      Else
         ptUnproj = ptInput
         ptProj = m_pcs.Transform(m_gcs, ptUnproj)
      End If

      'Display the projected and unprojected mouse coordinates.
      sbrStatus.Panels(1).Text = "X:" & VB6.Format(ptUnproj.X, "#0.000000")
      sbrStatus.Panels(2).Text = "Y:" & VB6.Format(ptUnproj.Y, "#0.000000")
   End Sub


   Private Sub txtApn_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtApn.TextChanged
      txtLong.Text = ""
      txtLat.Text = ""
   End Sub

   Private Sub showCoordinate(ByRef places As Collection)
      Dim ptInput As MapObjects2.Point
      Dim ptUnproj As MapObjects2.Point
      Dim strX, strY As String

      For Each ptInput In places
         'Display the projected and unprojected mouse coordinates.
         If Map1.Extent.Right > 500 Then
            ptUnproj = m_gcs.Transform(m_pcs, ptInput)
         Else
            ptUnproj = ptInput
         End If

         txtLong.Text = VB6.Format(ptUnproj.X, "#0.00000000")
         txtLat.Text = VB6.Format(ptUnproj.Y, "#0.00000000")

         strX = New String(" ", 32)
         strY = New String(" ", 32)
         Geo2Long(txtLong.Text, strX)
         Geo2Long(txtLat.Text, strY)
         txtLong2.Text = strX
         txtLat2.Text = strY

         'Exit For
      Next ptInput

   End Sub

   Private Sub txtCounty_TextChanged(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCounty.TextChanged
      '8/22/2014
      'VB6.SetDefault(cmdConnect, True)
      cmdConnect.Enabled = True
   End Sub

   Private Sub txtCounty_Leave(ByVal eventSender As System.Object, ByVal eventArgs As System.EventArgs) Handles txtCounty.Leave
      If g_sCounty <> txtCounty.Text Then
         g_sCounty = txtCounty.Text
         If Not initCounty(g_sCounty) Then
            MsgBox("Invalid county code: " & g_sCounty)
            Exit Sub
         End If
      End If

      txtGISBase.Text = g_sGISBase
      txtGISTable.Text = g_sGISTable
      txtGISIndex.Text = g_sGISIndex
      txtGeoCS.Text = CStr(g_iGeoCS)
      txtPrjCS.Text = CStr(g_iPrjCS)
      lblApnFormat.Text = "Format: " & atCountyInfo(g_iCntyIdx).sApnFormat
   End Sub

   Private Function doBuildIndex(sCounty As String, bOverwrite As Boolean) As Integer
      Dim layer As New MapObjects2.MapLayer
      Dim dc As New MapObjects2.DataConnection
      Dim pGd As MapObjects2.GeoDataset
      Dim iRet As Integer

      iRet = -1

      'Clean up existing image
      Call termGis()
      m_gcs = New MapObjects2.GeoCoordSys
      m_pcs = New MapObjects2.ProjCoordSys
      m_pl = New MapObjects2.PlaceLocator

      If g_sCounty <> sCounty Then
         g_sCounty = sCounty
         If Not initCounty(g_sCounty) Then
            MsgBox("Error initializing county: " & g_sCounty)
            GoTo cmdConnect_Exit
         End If

         txtGISBase.Text = g_sGISBase
         txtGISTable.Text = g_sGISTable
         txtGISIndex.Text = g_sGISIndex
         txtGeoCS.Text = CStr(g_iGeoCS)
         txtPrjCS.Text = CStr(g_iPrjCS)
      End If

      'Update variable
      g_sGISBase = txtGISBase.Text
      g_sGISTable = txtGISTable.Text
      g_sGISIndex = txtGISIndex.Text
      g_iGeoCS = CInt(txtGeoCS.Text)
      g_iPrjCS = CInt(txtPrjCS.Text)

      Me.Cursor = System.Windows.Forms.Cursors.WaitCursor
      dc.Database = g_sGISBase
      Try
         dc.Connect()
      Catch ex As Exception
         MsgBox("Error connecting to GIS system: " & ex.Message)
         iRet = 1
         GoTo cmdConnect_Exit
      End Try

      Try
         pGd = dc.FindGeoDataset(g_sGISTable)
         If pGd Is Nothing Then
            MsgBox("Cannot find Geo Dataset " & g_sGISTable)
            iRet = 2
            GoTo cmdConnect_Exit
         End If
      Catch ex As Exception
         MsgBox("Error connecting to GIS system: " & ex.Message)
         iRet = 2
         GoTo cmdConnect_Exit
      End Try

      m_pl.PlaceNameTable = pGd

      Try
         'Build index for search field - do not build if index already exist
         If Not m_pl.BuildIndex(g_sGISIndex, False) Then
            MsgBox("Error build index")
            iRet = 4
         Else
            iRet = 0
         End If
      Catch ex As Exception
         MsgBox("Error in build index: " & ex.Message)
         iRet = 3
      End Try


cmdConnect_Exit:
      'Clean up
      dc = Nothing
      layer = Nothing
      Me.Cursor = System.Windows.Forms.Cursors.Default
      doBuildIndex = iRet
   End Function

End Class