Option Strict Off
Option Explicit On
Module IniFileUtil
	
   Public Declare Function WritePrivateProfileString Lib "kernel32" Alias "WritePrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpString As String, ByVal lpFileName As String) As Integer
   Public Declare Function GetPrivateProfileString Lib "kernel32" Alias "GetPrivateProfileStringA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lpDefault As String, ByVal lpReturnedString As String, ByVal nSize As Integer, ByVal lpFileName As String) As Integer
   Public Declare Function GetPrivateProfileInt Lib "kernel32" Alias "GetPrivateProfileIntA" (ByVal lpApplicationName As String, ByVal lpKeyName As String, ByVal lDefault As Short, ByVal lpFileName As String) As Short
	Declare Function GetWindowsDirectory Lib "kernel32"  Alias "GetWindowsDirectoryA"(ByVal lpBuffer As String, ByVal nSize As Integer) As Integer
	
	Function GetIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As String
		Dim lpDefault As String
		Dim lpReturnString As String
		Dim size As Short
		Dim lpFileName As String
		Dim Length As Short
		
		lpDefault = ""
		lpReturnString = Space(256)
		size = Len(lpReturnString)
		lpFileName = My.Application.Info.DirectoryPath & "\" & lpApp & ".ini"
		Length = GetPrivateProfileString(lpSectionName, lpKeyName, lpDefault, lpReturnString, size, lpFileName)
		GetIniData = Left(lpReturnString, Length)
	End Function
	
	Function GetIniDataInt(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String) As Short
		Dim lpFileName As String
		
		lpFileName = My.Application.Info.DirectoryPath & "\" & lpApp & ".ini"
		GetIniDataInt = GetPrivateProfileInt(lpSectionName, lpKeyName, 0, lpFileName)
	End Function
	
	Function SaveIniData(ByRef lpApp As String, ByRef lpSectionName As String, ByRef lpKeyName As String, ByRef lpValue As String) As Boolean
      Dim lpFileName As String

		lpFileName = My.Application.Info.DirectoryPath & "\" & lpApp & ".ini"
		SaveIniData = WritePrivateProfileString(lpSectionName, lpKeyName, lpValue, lpFileName)
	End Function
End Module