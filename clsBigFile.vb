Option Strict Off
Option Explicit On
Friend Class clsBigFile
	
	Public Enum W32F_Errors
		W32F_UNKNOWN_ERROR = 45600
		W32F_FILE_ALREADY_OPEN
		W32F_PROBLEM_OPENING_FILE
		W32F_FILE_ALREADY_CLOSED
		W32F_Problem_seeking
	End Enum
	Private Structure TFileStat
      Public dwFileAttributes As Integer
      Public ftCreationTime As Integer
      Public ftLastAccessTime As Integer
      Public ftLastWriteTime As Integer
      Public nFileSize As Double
	End Structure
	
	Private Const W32F_SOURCE As String = "Win32File Object"
	
	Private Const GENERIC_WRITE As Integer = &H40000000
	Private Const GENERIC_READ As Integer = &H80000000
   Private Const FILE_ATTRIBUTE_NORMAL As Integer = &H80S
   Private Const FILE_SHARE_READ As Integer = &H1
   Private Const FILE_SHARE_WRITE As Integer = &H2
   Private Const FILE_FLAG_SEQUENTIAL_SCAN As Integer = &H8
   Private Const CREATE_ALWAYS As Integer = 2
   Private Const OPEN_EXISTING As Integer = 3
   Private Const OPEN_ALWAYS As Integer = 4
   Private Const INVALID_HANDLE_VALUE As Integer = -1
	
   Private Const FILE_BEGIN As Integer = 0
   Private Const FILE_CURRENT As Integer = 1
   Private Const FILE_END As Integer = 2
	
   Private Const FORMAT_MESSAGE_FROM_SYSTEM As Integer = &H1000S
   Declare Function FormatMessage Lib "kernel32" Alias "FormatMessageA" (ByVal dwFlags As Integer, ByRef lpSource As Integer, ByVal dwMessageId As Integer, ByVal dwLanguageId As Integer, ByVal lpBuffer As String, ByVal nSize As Integer, ByRef Arguments As VariantType) As Integer
   Declare Function ReadFile Lib "kernel32" (ByVal hFile As Integer, ByVal lpBuffer As String, ByVal nNumberOfBytesToRead As Integer, ByRef lpNumberOfBytesRead As Integer, ByVal lpOverlapped As Integer) As Integer
   Declare Function CloseHandle Lib "kernel32" (ByVal hObject As Integer) As Integer
   Declare Function WriteFile Lib "kernel32" (ByVal hFile As Integer, ByRef lpBuffer As String, ByVal nNumberOfBytesToWrite As Integer, ByRef lpNumberOfBytesWritten As Integer, ByVal lpOverlapped As Integer) As Integer
   Declare Function CreateFile Lib "kernel32" Alias "CreateFileA" (ByVal lpFileName As String, ByVal dwDesiredAccess As Integer, ByVal dwShareMode As Integer, ByVal lpSecurityAttributes As Integer, ByVal dwCreationDisposition As Integer, ByVal dwFlagsAndAttributes As Integer, ByVal hTemplateFile As Integer) As Integer
   Declare Function SetFilePointer Lib "kernel32" (ByVal hFile As Integer, ByVal lDistanceToMove As Integer, ByRef lpDistanceToMoveHigh As Integer, ByVal dwMoveMethod As Integer) As Integer
   Declare Function FlushFileBuffers Lib "kernel32" (ByVal hFile As Integer) As Integer
   Private Declare Function fileStat Lib "prodlib" (ByVal lpFileName As String, ByRef lpFileStat As TFileStat) As Integer
   Private Declare Sub myTimeString Lib "prodlib" Alias "timeString" (ByVal lpTime As String, ByVal lTime As Integer)

   Private hFile As Integer
   Private sFName As String
   Private fAutoFlush As Boolean

   Public ReadOnly Property FileHandle() As Integer
      Get
         If hFile = INVALID_HANDLE_VALUE Then
            RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
         End If
         FileHandle = hFile
      End Get
   End Property

   Public ReadOnly Property FileName() As String
      Get
         If hFile = INVALID_HANDLE_VALUE Then
            RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
         End If
         FileName = sFName
      End Get
   End Property

   Public ReadOnly Property IsOpen() As Boolean
      Get
         IsOpen = hFile <> INVALID_HANDLE_VALUE
      End Get
   End Property

   Public Property AutoFlush() As Boolean
      Get
         If hFile = INVALID_HANDLE_VALUE Then
            RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
         End If
         AutoFlush = fAutoFlush
      End Get
      Set(ByVal Value As Boolean)
         If hFile = INVALID_HANDLE_VALUE Then
            RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
         End If
         fAutoFlush = Value
      End Set
   End Property

   Public Sub OpenFile(ByVal sFileName As String, Optional ByVal sMode As String = "w")
      If hFile <> INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_OPEN, sFName)
      End If
      If sMode = "r" Then
         hFile = CreateFile(sFileName, GENERIC_READ, FILE_SHARE_READ, 0, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL Or FILE_FLAG_SEQUENTIAL_SCAN, 0)
      Else
         hFile = CreateFile(sFileName, GENERIC_WRITE Or GENERIC_READ, 0, 0, OPEN_ALWAYS, FILE_ATTRIBUTE_NORMAL, 0)
      End If
      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_PROBLEM_OPENING_FILE, sFileName)
      End If
      sFName = sFileName
   End Sub

   Public Sub CloseFile()
      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      CloseHandle(hFile)
      sFName = ""
      fAutoFlush = False
      hFile = INVALID_HANDLE_VALUE
   End Sub

   Public Function ReadRecs(ByVal ByteCount As Integer, ByRef strBuf As String) As Integer
      Dim BytesRead As Integer
      Dim Bytes As String = New String(CChar(" "), ByteCount)
      Dim bRet As Boolean

      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      BytesRead = 0
      Try
         bRet = ReadFile(hFile, Bytes, ByteCount, BytesRead, 0)
         strBuf = Bytes
      Catch ex As Exception
         'LogMsg("***** Error reading data in ReadRecs() " & vbCrLf & ex.Message)
         strBuf = ""
      End Try

      ReadRecs = BytesRead
   End Function

   'Return # bytes written
   Public Function WriteBytes(ByRef DataBytes() As Byte) As Integer
      Dim BytesToWrite, BytesWritten As Integer

      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      BytesToWrite = UBound(DataBytes) - LBound(DataBytes) + 1

      Try
         WriteFile(hFile, DataBytes(LBound(DataBytes)), BytesToWrite, BytesWritten, 0)
      Catch ex As Exception
         BytesWritten = 0
      End Try

      If fAutoFlush Then Flush()

      WriteBytes = BytesWritten
   End Function

   Public Sub Flush()
      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      FlushFileBuffers(hFile)
   End Sub

   Public Sub SeekAbsolute(ByVal HighPos As Integer, ByVal LowPos As Integer)
      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      LowPos = SetFilePointer(hFile, LowPos, HighPos, FILE_BEGIN)
   End Sub

   Public Sub SeekRelative(ByVal Offset As Integer)
      Dim TempLow, TempErr As Integer
      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      TempLow = SetFilePointer(hFile, Offset, 0, FILE_CURRENT)
      If TempLow = -1 Then
         TempErr = Err.LastDllError
         If TempErr Then
            RaiseError(W32F_Errors.W32F_Problem_seeking, "Error " & TempErr & "." & vbCrLf & CStr(TempErr))
         End If
      End If
   End Sub

   Public Function getFilePointer() As Double
      Dim TempErr As Integer
      Dim TempDbl As Double

      If hFile = INVALID_HANDLE_VALUE Then
         RaiseError(W32F_Errors.W32F_FILE_ALREADY_CLOSED)
      End If
      TempDbl = SetFilePointer(hFile, 0, 0, FILE_CURRENT)
      If TempDbl = -1 Then
         TempErr = Err.LastDllError
         If TempErr Then
            RaiseError(W32F_Errors.W32F_Problem_seeking, "Error " & TempErr & "." & vbCrLf & CStr(TempErr))
         End If
      End If
      getFilePointer = TempDbl
   End Function

   'Class_Initialize() is replaced by New() in VB2008
   Public Sub New()
      MyBase.New()

      'Your init data
      hFile = INVALID_HANDLE_VALUE
   End Sub

   'Class_Terminate() is replace by Finalize() in VB2008
   Protected Overrides Sub Finalize()
      If hFile <> INVALID_HANDLE_VALUE Then CloseHandle(hFile)
      MyBase.Finalize()
   End Sub

   Private Sub RaiseError(ByVal ErrorCode As W32F_Errors, Optional ByRef sExtra As String = "")
      Dim Win32Err As Integer
      Dim Win32Text As String
      Win32Err = Err.LastDllError

      If Win32Err Then
         Win32Text = vbCrLf & "Error " & Win32Err & vbCrLf & DecodeAPIErrors(Win32Err)
      Else
         Win32Text = ""
      End If

      Select Case ErrorCode
         Case W32F_Errors.W32F_FILE_ALREADY_OPEN
            Err.Raise(W32F_Errors.W32F_FILE_ALREADY_OPEN, W32F_SOURCE, "The file '" & sExtra & "' is already open." & Win32Text)
         Case W32F_Errors.W32F_PROBLEM_OPENING_FILE
            Err.Raise(W32F_Errors.W32F_PROBLEM_OPENING_FILE, W32F_SOURCE, "Error opening '" & sExtra & "'." & Win32Text)
         Case W32F_Errors.W32F_FILE_ALREADY_CLOSED
            Err.Raise(W32F_Errors.W32F_FILE_ALREADY_CLOSED, W32F_SOURCE, "There is no open file.")
         Case W32F_Errors.W32F_Problem_seeking
            Err.Raise(W32F_Errors.W32F_Problem_seeking, W32F_SOURCE, "Seek Error." & vbCrLf & sExtra)
         Case Else
            Err.Raise(W32F_Errors.W32F_UNKNOWN_ERROR, W32F_SOURCE, "Unknown error." & Win32Text)
      End Select
   End Sub
	
	Private Function DecodeAPIErrors(ByVal ErrorCode As Integer) As String
		Dim sMessage As String
		Dim MessageLength As Integer
		sMessage = Space(256)
		MessageLength = FormatMessage(FORMAT_MESSAGE_FROM_SYSTEM, 0, ErrorCode, 0, sMessage, 256, 0)
		If MessageLength > 0 Then
			DecodeAPIErrors = Left(sMessage, MessageLength)
		Else
			DecodeAPIErrors = "Unknown Error."
		End If
	End Function
	
   'This function return estimated number of records if recsize is prvided.
   'Otherwise, it returns actual file length
   Public Function getFileLength(ByVal strFilename As String, Optional ByVal lRecSize As Long = 0) As Double
      Dim myStat As TFileStat
      Dim dRet As Double

      myStat.nFileSize = 0
      dRet = fileStat(strFilename, myStat)
      If dRet = -1 Then
         getFileLength = 0
      Else
         If lRecSize > 0 Then
            getFileLength = myStat.nFileSize / lRecSize
         Else
            getFileLength = myStat.nFileSize
         End If
      End If
   End Function

	Public Function getFileCTime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileCTime = ""
		Else
         myTimeString(strTmp.Value, myStat.ftCreationTime)
			getFileCTime = strTmp.Value
		End If
	End Function
	
	Public Function getFileMTime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileMTime = ""
		Else
         myTimeString(strTmp.Value, myStat.ftLastWriteTime)
			getFileMTime = strTmp.Value
		End If
	End Function
	
	Public Function getFileATime(ByVal strFilename As String) As String
		Dim myStat As TFileStat
		Dim dRet As Double
		Dim strTmp As New VB6.FixedLengthString(32)
		
		dRet = fileStat(strFilename, myStat)
		If dRet = -1 Then
			getFileATime = ""
		Else
         myTimeString(strTmp.Value, myStat.ftLastAccessTime)
			getFileATime = strTmp.Value
		End If
	End Function
End Class