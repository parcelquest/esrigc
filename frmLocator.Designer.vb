<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()> Partial Class frmLocator
#Region "Windows Form Designer generated code "
	<System.Diagnostics.DebuggerNonUserCode()> Public Sub New()
		MyBase.New()
		'This call is required by the Windows Form Designer.
		InitializeComponent()
	End Sub
	'Form overrides dispose to clean up the component list.
	<System.Diagnostics.DebuggerNonUserCode()> Protected Overloads Overrides Sub Dispose(ByVal Disposing As Boolean)
		If Disposing Then
			If Not components Is Nothing Then
				components.Dispose()
			End If
		End If
		MyBase.Dispose(Disposing)
	End Sub
	'Required by the Windows Form Designer
	Private components As System.ComponentModel.IContainer
	Public ToolTip1 As System.Windows.Forms.ToolTip
	Public WithEvents sbrStatus As AxComctlLib.AxStatusBar
	Public WithEvents txtLong2 As System.Windows.Forms.TextBox
	Public WithEvents txtLat2 As System.Windows.Forms.TextBox
	Public WithEvents txtCounts As System.Windows.Forms.TextBox
	Public WithEvents List1 As System.Windows.Forms.ListBox
	Public WithEvents txtCounty As System.Windows.Forms.TextBox
	Public WithEvents cmdExit As System.Windows.Forms.Button
	Public WithEvents cmdBatch As System.Windows.Forms.Button
	Public WithEvents cmdLocateApn As System.Windows.Forms.Button
	Public WithEvents cmdConnect As System.Windows.Forms.Button
	Public WithEvents txtLat As System.Windows.Forms.TextBox
	Public WithEvents txtLong As System.Windows.Forms.TextBox
	Public WithEvents txtApn As System.Windows.Forms.TextBox
	Public WithEvents txtPrjCS As System.Windows.Forms.TextBox
	Public WithEvents txtGeoCS As System.Windows.Forms.TextBox
	Public WithEvents txtGISIndex As System.Windows.Forms.TextBox
	Public WithEvents txtGISTable As System.Windows.Forms.TextBox
	Public WithEvents txtGISBase As System.Windows.Forms.TextBox
	Public WithEvents Map1 As AxMapObjects2.AxMap
	Public WithEvents lblApnFormat As System.Windows.Forms.Label
	Public WithEvents Label9 As System.Windows.Forms.Label
	Public WithEvents Label8 As System.Windows.Forms.Label
	Public WithEvents Label7 As System.Windows.Forms.Label
	Public WithEvents Label6 As System.Windows.Forms.Label
	Public WithEvents Label5 As System.Windows.Forms.Label
	Public WithEvents Label4 As System.Windows.Forms.Label
	Public WithEvents Label3 As System.Windows.Forms.Label
	Public WithEvents Label2 As System.Windows.Forms.Label
	Public WithEvents Label1 As System.Windows.Forms.Label
	'NOTE: The following procedure is required by the Windows Form Designer
	'It can be modified using the Windows Form Designer.
	'Do not modify it using the code editor.
	<System.Diagnostics.DebuggerStepThrough()> Private Sub InitializeComponent()
		Dim resources As System.Resources.ResourceManager = New System.Resources.ResourceManager(GetType(frmLocator))
		Me.components = New System.ComponentModel.Container()
		Me.ToolTip1 = New System.Windows.Forms.ToolTip(components)
		Me.sbrStatus = New AxComctlLib.AxStatusBar
		Me.txtLong2 = New System.Windows.Forms.TextBox
		Me.txtLat2 = New System.Windows.Forms.TextBox
		Me.txtCounts = New System.Windows.Forms.TextBox
		Me.List1 = New System.Windows.Forms.ListBox
		Me.txtCounty = New System.Windows.Forms.TextBox
		Me.cmdExit = New System.Windows.Forms.Button
		Me.cmdBatch = New System.Windows.Forms.Button
		Me.cmdLocateApn = New System.Windows.Forms.Button
		Me.cmdConnect = New System.Windows.Forms.Button
		Me.txtLat = New System.Windows.Forms.TextBox
		Me.txtLong = New System.Windows.Forms.TextBox
		Me.txtApn = New System.Windows.Forms.TextBox
		Me.txtPrjCS = New System.Windows.Forms.TextBox
		Me.txtGeoCS = New System.Windows.Forms.TextBox
		Me.txtGISIndex = New System.Windows.Forms.TextBox
		Me.txtGISTable = New System.Windows.Forms.TextBox
		Me.txtGISBase = New System.Windows.Forms.TextBox
		Me.Map1 = New AxMapObjects2.AxMap
		Me.lblApnFormat = New System.Windows.Forms.Label
		Me.Label9 = New System.Windows.Forms.Label
		Me.Label8 = New System.Windows.Forms.Label
		Me.Label7 = New System.Windows.Forms.Label
		Me.Label6 = New System.Windows.Forms.Label
		Me.Label5 = New System.Windows.Forms.Label
		Me.Label4 = New System.Windows.Forms.Label
		Me.Label3 = New System.Windows.Forms.Label
		Me.Label2 = New System.Windows.Forms.Label
		Me.Label1 = New System.Windows.Forms.Label
		Me.SuspendLayout()
		Me.ToolTip1.Active = True
		CType(Me.sbrStatus, System.ComponentModel.ISupportInitialize).BeginInit()
		CType(Me.Map1, System.ComponentModel.ISupportInitialize).BeginInit()
		Me.Text = "GIS Geocode Application"
		Me.ClientSize = New System.Drawing.Size(802, 740)
		Me.Location = New System.Drawing.Point(4, 23)
		Me.Icon = CType(resources.GetObject("frmLocator.Icon"), System.Drawing.Icon)
		Me.StartPosition = System.Windows.Forms.FormStartPosition.WindowsDefaultLocation
		Me.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
		Me.BackColor = System.Drawing.SystemColors.Control
		Me.FormBorderStyle = System.Windows.Forms.FormBorderStyle.Sizable
		Me.ControlBox = True
		Me.Enabled = True
		Me.KeyPreview = False
		Me.MaximizeBox = True
		Me.MinimizeBox = True
		Me.Cursor = System.Windows.Forms.Cursors.Default
		Me.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.ShowInTaskbar = True
		Me.HelpButton = False
		Me.WindowState = System.Windows.Forms.FormWindowState.Normal
		Me.Name = "frmLocator"
		sbrStatus.OcxState = CType(resources.GetObject("sbrStatus.OcxState"), System.Windows.Forms.AxHost.State)
		Me.sbrStatus.Dock = System.Windows.Forms.DockStyle.Bottom
		Me.sbrStatus.Size = New System.Drawing.Size(802, 17)
		Me.sbrStatus.Location = New System.Drawing.Point(0, 723)
		Me.sbrStatus.TabIndex = 2
		Me.sbrStatus.Name = "sbrStatus"
		Me.txtLong2.AutoSize = False
		Me.txtLong2.Size = New System.Drawing.Size(105, 25)
		Me.txtLong2.Location = New System.Drawing.Point(560, 192)
		Me.txtLong2.TabIndex = 27
		Me.ToolTip1.SetToolTip(Me.txtLong2, "Longitude")
		Me.txtLong2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLong2.AcceptsReturn = True
		Me.txtLong2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLong2.BackColor = System.Drawing.SystemColors.Window
		Me.txtLong2.CausesValidation = True
		Me.txtLong2.Enabled = True
		Me.txtLong2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLong2.HideSelection = True
		Me.txtLong2.ReadOnly = False
		Me.txtLong2.Maxlength = 0
		Me.txtLong2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLong2.MultiLine = False
		Me.txtLong2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLong2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLong2.TabStop = True
		Me.txtLong2.Visible = True
		Me.txtLong2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLong2.Name = "txtLong2"
		Me.txtLat2.AutoSize = False
		Me.txtLat2.Size = New System.Drawing.Size(105, 25)
		Me.txtLat2.Location = New System.Drawing.Point(672, 192)
		Me.txtLat2.TabIndex = 26
		Me.ToolTip1.SetToolTip(Me.txtLat2, "Latitude")
		Me.txtLat2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLat2.AcceptsReturn = True
		Me.txtLat2.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLat2.BackColor = System.Drawing.SystemColors.Window
		Me.txtLat2.CausesValidation = True
		Me.txtLat2.Enabled = True
		Me.txtLat2.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLat2.HideSelection = True
		Me.txtLat2.ReadOnly = False
		Me.txtLat2.Maxlength = 0
		Me.txtLat2.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLat2.MultiLine = False
		Me.txtLat2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLat2.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLat2.TabStop = True
		Me.txtLat2.Visible = True
		Me.txtLat2.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLat2.Name = "txtLat2"
		Me.txtCounts.AutoSize = False
		Me.txtCounts.BackColor = System.Drawing.SystemColors.Menu
		Me.txtCounts.CausesValidation = False
		Me.txtCounts.Size = New System.Drawing.Size(105, 25)
		Me.txtCounts.Location = New System.Drawing.Point(672, 384)
		Me.txtCounts.ReadOnly = True
		Me.txtCounts.TabIndex = 25
		Me.ToolTip1.SetToolTip(Me.txtCounts, "Longitude")
		Me.txtCounts.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCounts.AcceptsReturn = True
		Me.txtCounts.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCounts.Enabled = True
		Me.txtCounts.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCounts.HideSelection = True
		Me.txtCounts.Maxlength = 0
		Me.txtCounts.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCounts.MultiLine = False
		Me.txtCounts.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCounts.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCounts.TabStop = True
		Me.txtCounts.Visible = True
		Me.txtCounts.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtCounts.Name = "txtCounts"
		Me.List1.Size = New System.Drawing.Size(137, 228)
		Me.List1.Location = New System.Drawing.Point(656, 424)
		Me.List1.TabIndex = 24
		Me.List1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.List1.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.List1.BackColor = System.Drawing.SystemColors.Window
		Me.List1.CausesValidation = True
		Me.List1.Enabled = True
		Me.List1.ForeColor = System.Drawing.SystemColors.WindowText
		Me.List1.IntegralHeight = True
		Me.List1.Cursor = System.Windows.Forms.Cursors.Default
		Me.List1.SelectionMode = System.Windows.Forms.SelectionMode.One
		Me.List1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.List1.Sorted = False
		Me.List1.TabStop = True
		Me.List1.Visible = True
		Me.List1.MultiColumn = False
		Me.List1.Name = "List1"
		Me.txtCounty.AutoSize = False
		Me.txtCounty.Font = New System.Drawing.Font("Arial", 12!, System.Drawing.FontStyle.Bold Or System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtCounty.Size = New System.Drawing.Size(49, 25)
		Me.txtCounty.Location = New System.Drawing.Point(608, 16)
		Me.txtCounty.TabIndex = 1
		Me.txtCounty.Text = "EDX"
		Me.ToolTip1.SetToolTip(Me.txtCounty, "Enter APN to search")
		Me.txtCounty.AcceptsReturn = True
		Me.txtCounty.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtCounty.BackColor = System.Drawing.SystemColors.Window
		Me.txtCounty.CausesValidation = True
		Me.txtCounty.Enabled = True
		Me.txtCounty.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtCounty.HideSelection = True
		Me.txtCounty.ReadOnly = False
		Me.txtCounty.Maxlength = 0
		Me.txtCounty.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtCounty.MultiLine = False
		Me.txtCounty.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtCounty.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtCounty.TabStop = True
		Me.txtCounty.Visible = True
		Me.txtCounty.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtCounty.Name = "txtCounty"
		Me.cmdExit.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdExit.Text = "E&xit"
		Me.cmdExit.Size = New System.Drawing.Size(105, 33)
		Me.cmdExit.Location = New System.Drawing.Point(672, 672)
		Me.cmdExit.TabIndex = 22
		Me.ToolTip1.SetToolTip(Me.cmdExit, "Exit")
		Me.cmdExit.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdExit.BackColor = System.Drawing.SystemColors.Control
		Me.cmdExit.CausesValidation = True
		Me.cmdExit.Enabled = True
		Me.cmdExit.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdExit.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdExit.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdExit.TabStop = True
		Me.cmdExit.Name = "cmdExit"
		Me.cmdBatch.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdBatch.Text = "&Batch"
		Me.cmdBatch.Enabled = False
		Me.cmdBatch.Size = New System.Drawing.Size(105, 25)
		Me.cmdBatch.Location = New System.Drawing.Point(672, 344)
		Me.cmdBatch.TabIndex = 21
		Me.ToolTip1.SetToolTip(Me.cmdBatch, "Generate list of Long/Lat point")
		Me.cmdBatch.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdBatch.BackColor = System.Drawing.SystemColors.Control
		Me.cmdBatch.CausesValidation = True
		Me.cmdBatch.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdBatch.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdBatch.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdBatch.TabStop = True
		Me.cmdBatch.Name = "cmdBatch"
		Me.cmdLocateApn.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdLocateApn.Text = "Locate &APN"
		Me.cmdLocateApn.Enabled = False
		Me.cmdLocateApn.Size = New System.Drawing.Size(105, 25)
		Me.cmdLocateApn.Location = New System.Drawing.Point(672, 304)
		Me.cmdLocateApn.TabIndex = 20
		Me.ToolTip1.SetToolTip(Me.cmdLocateApn, "Locate APN")
		Me.cmdLocateApn.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdLocateApn.BackColor = System.Drawing.SystemColors.Control
		Me.cmdLocateApn.CausesValidation = True
		Me.cmdLocateApn.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdLocateApn.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdLocateApn.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdLocateApn.TabStop = True
		Me.cmdLocateApn.Name = "cmdLocateApn"
		Me.cmdConnect.TextAlign = System.Drawing.ContentAlignment.MiddleCenter
		Me.cmdConnect.Text = "&Connect GIS DB"
		Me.AcceptButton = Me.cmdConnect
		Me.cmdConnect.Size = New System.Drawing.Size(105, 25)
		Me.cmdConnect.Location = New System.Drawing.Point(672, 264)
		Me.cmdConnect.TabIndex = 19
		Me.ToolTip1.SetToolTip(Me.cmdConnect, "Connect to GIS database")
		Me.cmdConnect.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.cmdConnect.BackColor = System.Drawing.SystemColors.Control
		Me.cmdConnect.CausesValidation = True
		Me.cmdConnect.Enabled = True
		Me.cmdConnect.ForeColor = System.Drawing.SystemColors.ControlText
		Me.cmdConnect.Cursor = System.Windows.Forms.Cursors.Default
		Me.cmdConnect.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.cmdConnect.TabStop = True
		Me.cmdConnect.Name = "cmdConnect"
		Me.txtLat.AutoSize = False
		Me.txtLat.Size = New System.Drawing.Size(105, 25)
		Me.txtLat.Location = New System.Drawing.Point(672, 160)
		Me.txtLat.TabIndex = 16
		Me.ToolTip1.SetToolTip(Me.txtLat, "Latitude")
		Me.txtLat.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLat.AcceptsReturn = True
		Me.txtLat.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLat.BackColor = System.Drawing.SystemColors.Window
		Me.txtLat.CausesValidation = True
		Me.txtLat.Enabled = True
		Me.txtLat.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLat.HideSelection = True
		Me.txtLat.ReadOnly = False
		Me.txtLat.Maxlength = 0
		Me.txtLat.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLat.MultiLine = False
		Me.txtLat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLat.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLat.TabStop = True
		Me.txtLat.Visible = True
		Me.txtLat.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLat.Name = "txtLat"
		Me.txtLong.AutoSize = False
		Me.txtLong.Size = New System.Drawing.Size(105, 25)
		Me.txtLong.Location = New System.Drawing.Point(560, 160)
		Me.txtLong.TabIndex = 15
		Me.ToolTip1.SetToolTip(Me.txtLong, "Longitude")
		Me.txtLong.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtLong.AcceptsReturn = True
		Me.txtLong.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtLong.BackColor = System.Drawing.SystemColors.Window
		Me.txtLong.CausesValidation = True
		Me.txtLong.Enabled = True
		Me.txtLong.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtLong.HideSelection = True
		Me.txtLong.ReadOnly = False
		Me.txtLong.Maxlength = 0
		Me.txtLong.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtLong.MultiLine = False
		Me.txtLong.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtLong.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtLong.TabStop = True
		Me.txtLong.Visible = True
		Me.txtLong.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtLong.Name = "txtLong"
		Me.txtApn.AutoSize = False
		Me.txtApn.Size = New System.Drawing.Size(217, 25)
		Me.txtApn.Location = New System.Drawing.Point(560, 64)
		Me.txtApn.TabIndex = 3
		Me.txtApn.Text = "0030113008"
		Me.ToolTip1.SetToolTip(Me.txtApn, "Enter APN to search")
		Me.txtApn.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtApn.AcceptsReturn = True
		Me.txtApn.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtApn.BackColor = System.Drawing.SystemColors.Window
		Me.txtApn.CausesValidation = True
		Me.txtApn.Enabled = True
		Me.txtApn.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtApn.HideSelection = True
		Me.txtApn.ReadOnly = False
		Me.txtApn.Maxlength = 0
		Me.txtApn.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtApn.MultiLine = False
		Me.txtApn.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtApn.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtApn.TabStop = True
		Me.txtApn.Visible = True
		Me.txtApn.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtApn.Name = "txtApn"
		Me.txtPrjCS.AutoSize = False
		Me.txtPrjCS.Size = New System.Drawing.Size(377, 27)
		Me.txtPrjCS.Location = New System.Drawing.Point(152, 208)
		Me.txtPrjCS.TabIndex = 13
		Me.txtPrjCS.Text = "Text1"
		Me.txtPrjCS.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtPrjCS.AcceptsReturn = True
		Me.txtPrjCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtPrjCS.BackColor = System.Drawing.SystemColors.Window
		Me.txtPrjCS.CausesValidation = True
		Me.txtPrjCS.Enabled = True
		Me.txtPrjCS.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtPrjCS.HideSelection = True
		Me.txtPrjCS.ReadOnly = False
		Me.txtPrjCS.Maxlength = 0
		Me.txtPrjCS.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtPrjCS.MultiLine = False
		Me.txtPrjCS.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtPrjCS.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtPrjCS.TabStop = True
		Me.txtPrjCS.Visible = True
		Me.txtPrjCS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtPrjCS.Name = "txtPrjCS"
		Me.txtGeoCS.AutoSize = False
		Me.txtGeoCS.Size = New System.Drawing.Size(377, 27)
		Me.txtGeoCS.Location = New System.Drawing.Point(152, 160)
		Me.txtGeoCS.TabIndex = 11
		Me.txtGeoCS.Text = "Text1"
		Me.txtGeoCS.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtGeoCS.AcceptsReturn = True
		Me.txtGeoCS.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGeoCS.BackColor = System.Drawing.SystemColors.Window
		Me.txtGeoCS.CausesValidation = True
		Me.txtGeoCS.Enabled = True
		Me.txtGeoCS.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGeoCS.HideSelection = True
		Me.txtGeoCS.ReadOnly = False
		Me.txtGeoCS.Maxlength = 0
		Me.txtGeoCS.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGeoCS.MultiLine = False
		Me.txtGeoCS.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGeoCS.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGeoCS.TabStop = True
		Me.txtGeoCS.Visible = True
		Me.txtGeoCS.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtGeoCS.Name = "txtGeoCS"
		Me.txtGISIndex.AutoSize = False
		Me.txtGISIndex.Size = New System.Drawing.Size(377, 27)
		Me.txtGISIndex.Location = New System.Drawing.Point(152, 112)
		Me.txtGISIndex.TabIndex = 9
		Me.txtGISIndex.Text = "Text1"
		Me.txtGISIndex.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtGISIndex.AcceptsReturn = True
		Me.txtGISIndex.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGISIndex.BackColor = System.Drawing.SystemColors.Window
		Me.txtGISIndex.CausesValidation = True
		Me.txtGISIndex.Enabled = True
		Me.txtGISIndex.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGISIndex.HideSelection = True
		Me.txtGISIndex.ReadOnly = False
		Me.txtGISIndex.Maxlength = 0
		Me.txtGISIndex.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGISIndex.MultiLine = False
		Me.txtGISIndex.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGISIndex.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGISIndex.TabStop = True
		Me.txtGISIndex.Visible = True
		Me.txtGISIndex.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtGISIndex.Name = "txtGISIndex"
		Me.txtGISTable.AutoSize = False
		Me.txtGISTable.Size = New System.Drawing.Size(377, 27)
		Me.txtGISTable.Location = New System.Drawing.Point(152, 64)
		Me.txtGISTable.TabIndex = 8
		Me.txtGISTable.Text = "Text1"
		Me.txtGISTable.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtGISTable.AcceptsReturn = True
		Me.txtGISTable.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGISTable.BackColor = System.Drawing.SystemColors.Window
		Me.txtGISTable.CausesValidation = True
		Me.txtGISTable.Enabled = True
		Me.txtGISTable.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGISTable.HideSelection = True
		Me.txtGISTable.ReadOnly = False
		Me.txtGISTable.Maxlength = 0
		Me.txtGISTable.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGISTable.MultiLine = False
		Me.txtGISTable.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGISTable.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGISTable.TabStop = True
		Me.txtGISTable.Visible = True
		Me.txtGISTable.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtGISTable.Name = "txtGISTable"
		Me.txtGISBase.AutoSize = False
		Me.txtGISBase.Size = New System.Drawing.Size(377, 27)
		Me.txtGISBase.Location = New System.Drawing.Point(152, 16)
		Me.txtGISBase.TabIndex = 7
		Me.txtGISBase.Text = "Text1"
		Me.txtGISBase.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.txtGISBase.AcceptsReturn = True
		Me.txtGISBase.TextAlign = System.Windows.Forms.HorizontalAlignment.Left
		Me.txtGISBase.BackColor = System.Drawing.SystemColors.Window
		Me.txtGISBase.CausesValidation = True
		Me.txtGISBase.Enabled = True
		Me.txtGISBase.ForeColor = System.Drawing.SystemColors.WindowText
		Me.txtGISBase.HideSelection = True
		Me.txtGISBase.ReadOnly = False
		Me.txtGISBase.Maxlength = 0
		Me.txtGISBase.Cursor = System.Windows.Forms.Cursors.IBeam
		Me.txtGISBase.MultiLine = False
		Me.txtGISBase.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.txtGISBase.ScrollBars = System.Windows.Forms.ScrollBars.None
		Me.txtGISBase.TabStop = True
		Me.txtGISBase.Visible = True
		Me.txtGISBase.BorderStyle = System.Windows.Forms.BorderStyle.Fixed3D
		Me.txtGISBase.Name = "txtGISBase"
		Map1.OcxState = CType(resources.GetObject("Map1.OcxState"), System.Windows.Forms.AxHost.State)
		Me.Map1.Size = New System.Drawing.Size(617, 441)
		Me.Map1.Location = New System.Drawing.Point(24, 264)
		Me.Map1.TabIndex = 0
		Me.Map1.Name = "Map1"
		Me.lblApnFormat.Text = "Format: "
		Me.lblApnFormat.Size = New System.Drawing.Size(217, 17)
		Me.lblApnFormat.Location = New System.Drawing.Point(560, 96)
		Me.lblApnFormat.TabIndex = 28
		Me.lblApnFormat.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.lblApnFormat.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.lblApnFormat.BackColor = System.Drawing.SystemColors.Control
		Me.lblApnFormat.Enabled = True
		Me.lblApnFormat.ForeColor = System.Drawing.SystemColors.ControlText
		Me.lblApnFormat.Cursor = System.Windows.Forms.Cursors.Default
		Me.lblApnFormat.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.lblApnFormat.UseMnemonic = True
		Me.lblApnFormat.Visible = True
		Me.lblApnFormat.AutoSize = False
		Me.lblApnFormat.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.lblApnFormat.Name = "lblApnFormat"
		Me.Label9.Text = "County"
		Me.Label9.Size = New System.Drawing.Size(41, 17)
		Me.Label9.Location = New System.Drawing.Point(560, 24)
		Me.Label9.TabIndex = 23
		Me.Label9.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label9.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label9.BackColor = System.Drawing.SystemColors.Control
		Me.Label9.Enabled = True
		Me.Label9.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label9.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label9.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label9.UseMnemonic = True
		Me.Label9.Visible = True
		Me.Label9.AutoSize = False
		Me.Label9.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label9.Name = "Label9"
		Me.Label8.Text = "Lat"
		Me.Label8.Size = New System.Drawing.Size(65, 17)
		Me.Label8.Location = New System.Drawing.Point(672, 144)
		Me.Label8.TabIndex = 18
		Me.Label8.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label8.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label8.BackColor = System.Drawing.SystemColors.Control
		Me.Label8.Enabled = True
		Me.Label8.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label8.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label8.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label8.UseMnemonic = True
		Me.Label8.Visible = True
		Me.Label8.AutoSize = False
		Me.Label8.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label8.Name = "Label8"
		Me.Label7.Text = "Long"
		Me.Label7.Size = New System.Drawing.Size(65, 17)
		Me.Label7.Location = New System.Drawing.Point(560, 144)
		Me.Label7.TabIndex = 17
		Me.Label7.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label7.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label7.BackColor = System.Drawing.SystemColors.Control
		Me.Label7.Enabled = True
		Me.Label7.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label7.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label7.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label7.UseMnemonic = True
		Me.Label7.Visible = True
		Me.Label7.AutoSize = False
		Me.Label7.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label7.Name = "Label7"
		Me.Label6.Text = "Enter APN"
		Me.Label6.Size = New System.Drawing.Size(177, 17)
		Me.Label6.Location = New System.Drawing.Point(560, 48)
		Me.Label6.TabIndex = 14
		Me.Label6.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label6.TextAlign = System.Drawing.ContentAlignment.TopLeft
		Me.Label6.BackColor = System.Drawing.SystemColors.Control
		Me.Label6.Enabled = True
		Me.Label6.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label6.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label6.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label6.UseMnemonic = True
		Me.Label6.Visible = True
		Me.Label6.AutoSize = False
		Me.Label6.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label6.Name = "Label6"
		Me.Label5.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label5.Text = "Prj Coordinate System"
		Me.Label5.Size = New System.Drawing.Size(129, 17)
		Me.Label5.Location = New System.Drawing.Point(8, 216)
		Me.Label5.TabIndex = 12
		Me.Label5.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label5.BackColor = System.Drawing.SystemColors.Control
		Me.Label5.Enabled = True
		Me.Label5.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label5.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label5.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label5.UseMnemonic = True
		Me.Label5.Visible = True
		Me.Label5.AutoSize = False
		Me.Label5.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label5.Name = "Label5"
		Me.Label4.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label4.Text = "Geo Coordinate System"
		Me.Label4.Size = New System.Drawing.Size(113, 17)
		Me.Label4.Location = New System.Drawing.Point(24, 168)
		Me.Label4.TabIndex = 10
		Me.Label4.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label4.BackColor = System.Drawing.SystemColors.Control
		Me.Label4.Enabled = True
		Me.Label4.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label4.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label4.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label4.UseMnemonic = True
		Me.Label4.Visible = True
		Me.Label4.AutoSize = False
		Me.Label4.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label4.Name = "Label4"
		Me.Label3.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label3.Text = "GIS Index"
		Me.Label3.Size = New System.Drawing.Size(113, 17)
		Me.Label3.Location = New System.Drawing.Point(24, 120)
		Me.Label3.TabIndex = 6
		Me.Label3.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label3.BackColor = System.Drawing.SystemColors.Control
		Me.Label3.Enabled = True
		Me.Label3.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label3.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label3.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label3.UseMnemonic = True
		Me.Label3.Visible = True
		Me.Label3.AutoSize = False
		Me.Label3.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label3.Name = "Label3"
		Me.Label2.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label2.Text = "GIS Table"
		Me.Label2.Size = New System.Drawing.Size(113, 17)
		Me.Label2.Location = New System.Drawing.Point(24, 72)
		Me.Label2.TabIndex = 5
		Me.Label2.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label2.BackColor = System.Drawing.SystemColors.Control
		Me.Label2.Enabled = True
		Me.Label2.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label2.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label2.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label2.UseMnemonic = True
		Me.Label2.Visible = True
		Me.Label2.AutoSize = False
		Me.Label2.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label2.Name = "Label2"
		Me.Label1.TextAlign = System.Drawing.ContentAlignment.TopRight
		Me.Label1.Text = "GIS Location"
		Me.Label1.Size = New System.Drawing.Size(113, 17)
		Me.Label1.Location = New System.Drawing.Point(24, 24)
		Me.Label1.TabIndex = 4
		Me.Label1.Font = New System.Drawing.Font("Arial", 8!, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, CType(0, Byte))
		Me.Label1.BackColor = System.Drawing.SystemColors.Control
		Me.Label1.Enabled = True
		Me.Label1.ForeColor = System.Drawing.SystemColors.ControlText
		Me.Label1.Cursor = System.Windows.Forms.Cursors.Default
		Me.Label1.RightToLeft = System.Windows.Forms.RightToLeft.No
		Me.Label1.UseMnemonic = True
		Me.Label1.Visible = True
		Me.Label1.AutoSize = False
		Me.Label1.BorderStyle = System.Windows.Forms.BorderStyle.None
		Me.Label1.Name = "Label1"
		Me.Controls.Add(sbrStatus)
		Me.Controls.Add(txtLong2)
		Me.Controls.Add(txtLat2)
		Me.Controls.Add(txtCounts)
		Me.Controls.Add(List1)
		Me.Controls.Add(txtCounty)
		Me.Controls.Add(cmdExit)
		Me.Controls.Add(cmdBatch)
		Me.Controls.Add(cmdLocateApn)
		Me.Controls.Add(cmdConnect)
		Me.Controls.Add(txtLat)
		Me.Controls.Add(txtLong)
		Me.Controls.Add(txtApn)
		Me.Controls.Add(txtPrjCS)
		Me.Controls.Add(txtGeoCS)
		Me.Controls.Add(txtGISIndex)
		Me.Controls.Add(txtGISTable)
		Me.Controls.Add(txtGISBase)
		Me.Controls.Add(Map1)
		Me.Controls.Add(lblApnFormat)
		Me.Controls.Add(Label9)
		Me.Controls.Add(Label8)
		Me.Controls.Add(Label7)
		Me.Controls.Add(Label6)
		Me.Controls.Add(Label5)
		Me.Controls.Add(Label4)
		Me.Controls.Add(Label3)
		Me.Controls.Add(Label2)
		Me.Controls.Add(Label1)
		CType(Me.Map1, System.ComponentModel.ISupportInitialize).EndInit()
		CType(Me.sbrStatus, System.ComponentModel.ISupportInitialize).EndInit()
		Me.ResumeLayout(False)
		Me.PerformLayout()
	End Sub
#End Region 
End Class