Option Strict Off
Option Explicit On
Module Parse
	
	Public Const sEmpty As String = ""
	
	Function GetToken(ByRef sTarget As String, ByRef sSeps As String) As String
		' Assume failure
		GetToken = sEmpty
		
		' Note that sSave and iStart must be static from call to call
		' If first call, make copy of string
		Static sSave As String
      Static iStart, cSave As Integer
		
		If sTarget <> sEmpty Then
			iStart = 1
			sSave = sTarget
			cSave = Len(sSave)
		Else
			If sSave = sEmpty Then Exit Function
		End If
		
		' Find start of next token
      Dim iNew As Integer
		iNew = StrSpan(sSave, iStart, sSeps)
		If iNew Then
			' Set position to start of token
			iStart = iNew
		Else
			' If no new token, return empty string
			Exit Function
		End If
		
		' Find end of token
		iNew = StrBreak(sSave, iStart, sSeps)
		If iNew = 0 Then
			' If no end of token, set to end of string
			iNew = cSave + 1
		End If
		
		' Cut token out of sTarget string
		GetToken = Mid(sSave, iStart, iNew - iStart)
		' Set new starting position
		iStart = iNew
	End Function
	
   Function StrBreak(ByRef sTarget As String, ByVal iStart As Integer, ByRef sSeps As String) As Integer
      Dim cTarget As Integer
      cTarget = Len(sTarget)

      ''@B StrBreak
      ' Look for end of token (first character that is a separator)
      Do While InStr(sSeps, Mid(sTarget, iStart, 1)) = 0
         ''@E StrBreak
         If iStart > cTarget Then
            StrBreak = 0
            Exit Function
         Else
            iStart = iStart + 1
         End If
      Loop
      StrBreak = iStart
   End Function
	
	''@B StrSpan
   Function StrSpan(ByRef sTarget As String, ByVal iStart As Integer, ByRef sSeps As String) As Integer

      Dim cTarget As Integer
      cTarget = Len(sTarget)
      ' Look for start of token (character that isn't a separator)
      Do While InStr(sSeps, Mid(sTarget, iStart, 1))
         If iStart > cTarget Then
            StrSpan = 0
            Exit Function
         Else
            iStart = iStart + 1
         End If
      Loop
      StrSpan = iStart

   End Function ''@E StrSpan
	
   Public Function fCount(ByRef s As String, ByRef sDel As String) As Integer
      Dim j As Integer
      Dim k As Integer

      j = 0 : k = 0

      j = InStr(s, sDel)
      Do While j <> 0
         k = k + 1
         j = InStr(j + 1, s, sDel)
      Loop
      fCount = k
   End Function
	
   Public Function ParseStr(ByRef sBuf As String, ByRef sDel As String, ByRef aStr() As String) As Integer
      Dim cnt, j As Integer
      Dim st As String

      st = sBuf
      cnt = fCount(st, sDel)
      ReDim aStr(cnt + 1)
      For j = 0 To cnt - 1
         aStr(j) = Mid(st, 1, InStr(st, sDel) - 1)
         st = Mid(st, InStr(st, sDel) + Len(sDel))
      Next
      aStr(j) = st
      ParseStr = cnt + 1
   End Function
	
   Public Function splitToken(ByRef sString As String, ByRef aTerms() As String, ByRef strDelim As String) As Integer
      Dim sTemp As String
      Dim sNewTerm As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer
      Dim iRet As Integer

      sTemp = Trim(sString)
      iLen = Len(sTemp)
      iRet = 0

      If sTemp = "" Then
         splitToken = 0
         GoTo lblExit
      End If

      ReDim aTerms(0)

      If Left(sTemp, 1) = strDelim Then
         iCurrentPos = InStr(2, sTemp, strDelim)
         If iCurrentPos = iLen Then
            iLastPos = iLen
         Else
            iLastPos = 2
         End If
      Else
         iCurrentPos = InStr(1, sTemp, strDelim)
         iLastPos = 1
      End If

      If iCurrentPos <= iLastPos Then
         aTerms(0) = sTemp
         iRet = 1
         GoTo lblExit
      End If

      Do Until iCurrentPos > iLen
         Do Until Mid(sTemp, iCurrentPos, 1) = strDelim
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
         Loop

         sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
         If Not (UBound(aTerms) = 0 And aTerms(UBound(aTerms)) = "") Then
            ReDim Preserve aTerms(UBound(aTerms) + 1)
         End If

         aTerms(UBound(aTerms)) = sNewTerm
         iLastPos = iCurrentPos + 1
         iRet = iRet + 1

         iCurrentPos = iLastPos
      Loop

lblExit:
      splitToken = iRet
      Exit Function

lblErr:
      LogMsg("Error in splitToken(): " & Err.Description)
      splitToken = 0
   End Function
	
   Public Function splitStrToken(ByRef sString As String, ByRef aTerms() As String, ByRef strDelim As String) As Integer
      Dim sTemp As String
      Dim sNewTerm As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer
      Dim iRet As Integer

      sTemp = Replace(Trim(sString), Chr(34) & Chr(34), "^")
      iLen = Len(sTemp)
      iRet = 0

      If sTemp = "" Then
         splitStrToken = 0
         GoTo lblExit
      End If

      ReDim aTerms(0)

      If Left(sTemp, 1) = strDelim Then
         iCurrentPos = InStr(2, sTemp, strDelim)
         If iCurrentPos = iLen Then
            iLastPos = iLen
         Else
            iLastPos = 2
         End If
      ElseIf Left(sTemp, 1) = Chr(34) Then
         iCurrentPos = InStr(2, sTemp, Chr(34)) + 1
         iLastPos = 1
      Else
         iCurrentPos = InStr(1, sTemp, strDelim)
         iLastPos = 1
      End If

      If iCurrentPos <= iLastPos Then
         aTerms(0) = sTemp
         iRet = 1
         GoTo lblExit
      End If

      Do Until iCurrentPos > iLen + 1
         If Mid(sTemp, iCurrentPos, 1) = Chr(34) Then
            iCurrentPos = iCurrentPos + 1
            iLastPos = iCurrentPos
            Do Until Mid(sTemp, iCurrentPos, 1) = Chr(34)
               iCurrentPos = iCurrentPos + 1
               If iCurrentPos > iLen Then Exit Do
            Loop
         Else
            Do Until Mid(sTemp, iCurrentPos, 1) = strDelim
               iCurrentPos = iCurrentPos + 1
               If iCurrentPos > iLen Then Exit Do
            Loop
         End If

         sNewTerm = Mid(sTemp, iLastPos, iCurrentPos - iLastPos)
         If Not (UBound(aTerms) = 0 And aTerms(UBound(aTerms)) = "") Then
            ReDim Preserve aTerms(UBound(aTerms) + 1)
         End If

         'Check for empty string
         If sNewTerm = "^" Then
            sNewTerm = ""
         End If
         aTerms(UBound(aTerms)) = Replace(sNewTerm, "^", Chr(34))
         If Mid(sTemp, iCurrentPos, 1) = Chr(34) Then
            iLastPos = iCurrentPos + 2
         Else
            iLastPos = iCurrentPos + 1
         End If
         iRet = iRet + 1
         iCurrentPos = iLastPos
      Loop

      aTerms(0) = stripQuote(aTerms(0))
lblExit:
      splitStrToken = iRet
      Exit Function

lblErr:
      LogMsg("Error in splitToken(): " & Err.Description)
      splitStrToken = 0
   End Function
	
	Public Sub makeCsv(ByRef alStr() As String, ByRef strBuf As String)
      Dim iTmp As Integer
		Dim strTmp As String
		
		strTmp = ""
		For iTmp = 0 To UBound(alStr) - 1
			strTmp = strTmp & alStr(iTmp) & ","
		Next 
		strBuf = Left(strTmp, Len(strTmp) - 1)
	End Sub
	
	Public Function getDblValue(ByRef sBuf As String) As Double
		Dim dRet As Double
		
		On Error Resume Next
		
		dRet = 0
		'Look for quotation mark
		If Left(sBuf, 1) = Chr(34) Then
			dRet = Val(Mid(sBuf, 2))
		Else
			dRet = Val(sBuf)
		End If
		
		getDblValue = dRet
   End Function

   Public Function blankRem(ByVal sString As String) As String
      Dim sTemp As String
      Dim iLen As Integer
      Dim iCurrentPos As Integer
      Dim iLastPos As Integer
      Dim sRet As String

      sTemp = Trim(sString)
      iLen = Len(sTemp)
      sRet = ""

      If sTemp = "" Then
         GoTo blankRem_Exit
      End If

      If Left(sTemp, 1) = " " Then
         iCurrentPos = InStr(2, sTemp, " ")
         If iCurrentPos = iLen Then
            iLastPos = iLen
         Else
            iLastPos = 2
         End If
      Else
         iCurrentPos = InStr(1, sTemp, " ")
         iLastPos = 1
      End If

      If iCurrentPos <= iLastPos Then
         sRet = sTemp
         GoTo blankRem_Exit
      End If

      Do Until iCurrentPos > iLen
         Do Until Mid(sTemp, iCurrentPos, 1) = " "
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
         Loop

         sRet = sRet & Mid(sTemp, iLastPos, iCurrentPos - iLastPos) & " "
         Do Until Mid(sTemp, iCurrentPos, 1) <> " "
            iCurrentPos = iCurrentPos + 1
            If iCurrentPos > iLen Then Exit Do
         Loop

         iLastPos = iCurrentPos
         iCurrentPos = iLastPos
      Loop

blankRem_Exit:
      blankRem = sRet
      Exit Function

blankRem_Err:
      LogMsg("Error in blankRem(): " & Err.Description)
      blankRem = ""
   End Function

End Module
