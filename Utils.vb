Option Strict Off
Option Explicit On
Module Utils
	
	Public Structure ADDR1_DEF
		Dim strNum As String
		Dim strSub As String
		Dim strDir As String
		Dim strName As String
		Dim strSfx As String
		Dim strUnit As String
	End Structure
	Public Structure ADDR2_DEF
		Dim sCity As String
		Dim sState As String
		Dim sZip As String
		Dim sZip4 As String
	End Structure
	
   Private Const M_STRNUM_OFF As Integer = 1
   Private Const M_STRSUB_OFF As Integer = 8
   Private Const M_STRDIR_OFF As Integer = 11
   Private Const M_STRNAM_OFF As Integer = 13
   Private Const M_STRSFX_OFF As Integer = 37
   Private Const M_STRUNIT_OFF As Integer = 42
   Private Const M_CITY_OFF As Integer = 1
   Private Const M_STATE_OFF As Integer = 18
   Private Const M_ZIP_OFF As Integer = 20
   Private Const M_ZIP4_OFF As Integer = 25
	
	Public g_logFile As String
	
	Public Function readTextFile(ByRef fileName As String) As String
		Dim strBuf As String
		Dim strTmp As String
      Dim fd As Integer
		
		fd = FreeFile
		FileOpen(fd, fileName, OpenMode.Input)
		strBuf = ""
		
		Do While Not EOF(fd)
			strTmp = LineInput(fd)
			strBuf = strBuf & strTmp
		Loop 
		
		FileClose(fd)
		readTextFile = strBuf
	End Function
	
	Public Sub LogMsg(ByVal strMsg As String)
      Dim ff As Integer
		
		On Error GoTo ErrorHandler
		ff = FreeFile
		
		FileOpen(ff, g_logFile, OpenMode.Append)
		PrintLine(ff, Now & vbTab & strMsg)
		FileClose(ff)
		Exit Sub
		
ErrorHandler: 
		'MsgBox "Please check for diskspace on " & g_logFile
	End Sub
	
	Public Sub LogMsg0(ByVal strMsg As String)
      Dim ff As Integer
		
		On Error GoTo ErrorHandler
		ff = FreeFile
		
		FileOpen(ff, g_logFile, OpenMode.Append)
		PrintLine(ff, strMsg)
		FileClose(ff)
		
ErrorHandler: 
	End Sub
	
   Public Function FormatData(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp As String

      If Field = "" Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
      End If

      FormatData = ""
      Select Case DataType
         Case "CHARACTER"
            If fldLen >= Length Then
               FormatData = Left(fldData, Length)
            Else
               FormatData = IIf(fldLen = 0, New String(" ", Length), fldData & New String(" ", Length - fldLen))
            End If
         Case "NUMERIC"
            FormatData = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "DATE"
            FormatData = IIf(fldLen = 0, New String(" ", 8), VB6.Format(fldData, "YYYYMMDD"))
         Case "DATE8"
            strTmp = IIf(fldLen = 0, New String(" ", 8), VB6.Format(fldData, "MMDDYYYY"))
            If strTmp = "01011900" Or strTmp = "01011800" Then
               FormatData = New String(" ", 8)
            Else
               FormatData = strTmp
            End If
         Case "DATE6"
            'strTmp = IIf(IsNull(Field), String$(6, " "), Format$(fldData, "MMYYYY"))
            If fldLen = 0 Or fldLen > 6 Then
               strTmp = IIf(fldLen = 0, New String(" ", 6), VB6.Format(fldData, "MMYYYY"))
            Else
               strTmp = fldData
            End If
            If strTmp = "011900" Or strTmp = "011800" Then
               strTmp = New String(" ", 6)
            End If
            FormatData = strTmp
      End Select
   End Function

   Public Function FormatData1(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp, strTmp1 As String
      Dim lTmp As Integer

      On Error GoTo FormatData1_Error

      If fldLen = 0 Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
      End If

      Select Case DataType
         Case "C"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), fldData & Space(Length - fldLen))
            End If
         Case "D"
            strTmp = IIf(fldLen = 0, Space(8), VB6.Format(fldData, "YYYYMMDD"))
         Case "D1" 'YYYYMMDD -> MMYY
            If fldLen <> 8 Then
               strTmp = New String("0", 4)
            Else
               strTmp = Mid(fldData, 5, 2) & Mid(fldData, 3, 2)
            End If
         Case "D2" 'YYYYMMDD -> MMDDYY
            If fldLen <> 8 Then
               strTmp = New String("0", 6)
            Else
               strTmp = Mid(fldData, 5, 4) & Mid(fldData, 3, 2)
            End If
         Case "D3" 'MMDDYYYY -> MMYY
            If fldLen <> 8 Then
               strTmp = Space(4)
            Else
               strTmp = Mid(fldData, 5, 2) & Mid(fldData, 3, 2)
            End If
         Case "D4" 'MMDDYYYY -> MMDDYY
            If fldLen <> 8 Then
               strTmp = Space(6)
            Else
               strTmp = Left(fldData, 4) & Right(fldData, 2)
            End If
         Case "D6", "DATE6"
            If fldLen = 0 Or fldLen > 6 Then
               strTmp = IIf(fldLen = 0, Space(6), VB6.Format(fldData, "MMYYYY"))
            Else
               strTmp = fldData
            End If
            If strTmp = "011900" Or strTmp = "011800" Then
               strTmp = Space(6)
            End If
         Case "D8"
            strTmp = IIf(fldLen = 0, Space(8), VB6.Format(fldData, "MMDDYYYY"))
            If strTmp = "01011900" Or strTmp = "01011800" Then
               strTmp = Space(8)
            Else
               strTmp = strTmp
            End If
         Case "L"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), fldData & Space(Length - fldLen))
            End If
         Case "L0"
            If fldLen >= Length Then
               strTmp = Left(fldData, Length)
            Else               
               strTmp = IIf(fldLen = 0, New String("0", Length), fldData & New String("0", Length - fldLen))
            End If
         Case "R"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, Space(Length), Space(Length - fldLen) & fldData)
            End If
         Case "R0"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "Z"
            If fldLen > Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "Z1"
            If fldLen > 0 Then
               lTmp = 10 * Val(Field)
               fldData = CStr(lTmp)
               fldLen = Len(fldData)
            End If
            strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "Z2"
            If fldLen > 1 Then
               lTmp = 100 * Val(Field)
               fldData = CStr(lTmp)
               fldLen = Len(fldData)
            End If
            strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
         Case "ZB"
            If fldLen > 1 Then
               lTmp = Val(Field)
               If lTmp = 0 Then
                  fldData = " "
                  fldLen = 1
               End If
            End If
            strTmp = IIf(fldLen = 0, Space(Length), Space(Length - fldLen) & fldData)
         Case "ZR"
            If fldLen >= Length Then
               strTmp = Right(fldData, Length)
            Else
               strTmp = IIf(fldLen = 0, New String("0", Length), New String("0", Length - fldLen) & fldData)
            End If
         Case "F" 'Filler
            strTmp = New String("0", Length)
         Case Else 'Filler
            strTmp = New String("*", Length)
      End Select
      FormatData1 = strTmp
      Exit Function

FormatData1_Error:
      MsgBox("Error: " & Err.Description)
      FormatData1 = fldData
   End Function

   Public Function FormatDisplay(ByRef Length As Integer, ByRef DataType As String, ByRef Field As String) As String
      Dim fldLen As Integer
      Dim fldData As String
      Dim strTmp, strTmp1 As String
      Dim lTmp As Integer
      Dim dTmp As Double

      On Error GoTo FormatDisplay_Error

      If Field = "" Then
         fldLen = 0
         fldData = ""
      Else
         fldData = Trim(Field)
         fldLen = Len(fldData)
         If (fldLen > 4) And (fldData = New String("9", fldLen)) Then
            fldData = ""
            fldLen = 0
         End If
      End If

      Select Case DataType
         Case "$"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               strTmp1 = VB6.Format(fldData, "$##,##")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "$100"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               lTmp = Val(fldData)
               dTmp = lTmp / 100
               strTmp1 = VB6.Format(dTmp, "$##,##.00")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "A" 'Format acreage
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               If fldLen > 3 Then
                  strTmp1 = "." & Right(fldData, 3)
                  fldData = Left(fldData, Len(fldData) - 3)
                  fldLen = Len(fldData)
                  If fldLen > 3 Then
                     strTmp1 = Left(fldData, fldLen - 3) & "," & Right(fldData, 3) & strTmp1
                  Else
                     strTmp1 = fldData & strTmp1
                  End If
               Else
                  strTmp1 = "0." & fldData & New String("0", 3 - fldLen)
               End If
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If
         Case "D" 'YYYYMMDD -> MM/DD/YYYY
            If fldLen <> 8 Then
               strTmp = Space(10)
            Else
               strTmp = Mid(fldData, 5, 2) & "/" & Mid(fldData, 7, 2) & "/" & Left(fldData, 4)
            End If
         Case "N"
            If fldLen = 0 Then
               strTmp = Space(Length)
            Else
               strTmp1 = VB6.Format(fldData, "##,##")
               strTmp = Space(Length - Len(strTmp1)) & strTmp1
            End If

         Case Else 'Filler
            strTmp = New String("*", Length)
      End Select
      FormatDisplay = strTmp
      Exit Function

FormatDisplay_Error:
      MsgBox("Error: " & Err.Description)
      FormatDisplay = fldData
   End Function
	
   Public Function FormatMAddr1(ByRef Length As Integer, ByRef DataType As String, ByRef Field As ADDR1_DEF) As String
      Dim strTmp, strTmp1 As String

      On Error GoTo FormatMAddr1_Err

      If Len(Field.strName) = 0 Then
         strTmp = Space(Length)
      Else
         strTmp1 = ""
         If Field.strNum <> "" Then
            strTmp1 = Field.strNum & " "
         End If
         If Field.strDir <> "" Then
            strTmp1 = strTmp1 & Field.strDir & " "
         End If

         strTmp1 = strTmp1 & Field.strName & " "

         If Field.strSfx <> "" Then
            strTmp1 = strTmp1 & Field.strSfx & " "
         End If
         If Field.strUnit <> "" Then
            strTmp1 = strTmp1 & "#" & Field.strUnit
         End If

         strTmp = strTmp1 & Space(Length - Len(strTmp1))
      End If

      FormatMAddr1 = strTmp
      Exit Function

FormatMAddr1_Err:
      FormatMAddr1 = Space(Length)
   End Function
	
   Public Function FormatMAddr2(ByRef Length As Integer, ByRef DataType As String, ByRef Field As ADDR2_DEF) As String
      Dim strTmp, strTmp1 As String
      Dim lZip As Integer

      On Error GoTo FormatMAddr2_Err

      If Len(Field.sCity) = 0 Then
         strTmp = Space(Length)
      Else
         strTmp1 = Field.sCity & " "
         If Field.sState <> "" Then
            strTmp1 = strTmp1 & Field.sState & " "
         End If
         lZip = Val(Field.sZip)
         If lZip > 0 Then
            strTmp1 = strTmp1 & Field.sZip

            lZip = Val(Field.sZip4)
            If lZip > 0 Then
               strTmp1 = strTmp1 & "-" & Field.sZip4
            End If
         End If

         strTmp = strTmp1 & Space(Length - Len(strTmp1))
      End If

      FormatMAddr2 = strTmp
      Exit Function

FormatMAddr2_Err:
      FormatMAddr2 = Space(Length)
   End Function
	
	Public Sub dispMsg(ByRef strMsg As String, ByRef bLogOnly As Boolean)
		If bLogOnly Then
			LogMsg(strMsg)
		Else
			MsgBox(strMsg)
		End If
	End Sub
   '<DllImport("kernel32.dll", CharSet:=CharSet.Auto, SetLastError:=True)> _
   'Public Shared Function OpenProcess(ByVal access As Integer, ByVal inherit As Boolean, ByVal processId As Integer) As SafeProcessHandle
   'End Function
   Public Sub WaitOnProgram(ByVal hInst As Long, Optional ByVal lSecs As Long = 3600000)
      'Dim hProc As Long
      'Dim iRet As Long

      'hProc = OpenProcess(PROCESS_ALL_ACCESS, False, hInst)
      'If hProc <> hNull Then
      '   iRet = WaitForSingleObject(hProc, lSecs)      'Give it 1 hour to run
      '   CloseHandle(hProc)
      'End If
   End Sub
   Public Function removeBlank(ByRef sStr As String) As String
      removeBlank = sStr
   End Function
	
   Public Function removeBlankAt(ByRef sStr As String, ByRef iPos As Integer) As String
      Dim sTmp As String

      If Mid(sStr, iPos, 1) = " " Then
         sTmp = Left(sStr, iPos - 1) & Mid(sStr, iPos + 1)
      Else
         sTmp = sStr
      End If
      removeBlankAt = RTrim(sTmp)
   End Function
	
	Public Function removeLeadingZero(ByRef sStr As String) As String
      Dim biTmp As Double
		
		biTmp = CDbl("" & sStr)
		removeLeadingZero = CStr(biTmp)
	End Function
	
	Public Function stripQuote(ByRef sStr As String) As String
		Dim sTmp As String
		
		If Left(sStr, 1) = Chr(34) Then
			sTmp = Mid(sStr, 2, Len(sStr) - 2)
		Else
			sTmp = sStr
		End If
		
		stripQuote = sTmp
	End Function
	
	Public Function getPath(ByRef sPathName As String) As String
      Dim iPos As Integer
		Dim sPath As String
		
		On Error GoTo getPath_Err
		
		sPath = ""
		If sPathName <> "" Then
			iPos = InStrRev(sPathName, "\")
			If iPos > 1 Then
				sPath = Left(sPathName, iPos)
			End If
		End If
		
		getPath = sPath
		Exit Function
getPath_Err: 
		getPath = ""
	End Function
End Module