Option Strict Off
Option Explicit On
Imports VB = Microsoft.VisualBasic
Module modMain

   Public Const MAX_CNTYS As Short = 70
   Public g_sCounty As String

   Public g_sGISBase As String
   Public g_sGISTable As String
   Public g_sGISIndex As String
   Public g_iGeoCS As Integer
   Public g_iPrjCS As Integer
   Public g_iRecSize As Short
   Public g_srcTmpl As String
   Public g_dstTmpl As String
   Public g_srcFile As String
   Public g_dstFile As String
   Public g_sLogPath As String
   Public g_iApnLen As Short
   Public g_bStop As Boolean
   Public g_bReplaceOnly As Boolean
   'Public g_bFormatApn As Boolean
   Public g_iApnFormat As Short
   Public g_iGeoPos As Short
   'Public g_iUseSitus As Integer
   'Public g_iUseMail As Integer
   'Public g_iUseAcrage As Integer
   Public g_iUseGis As Integer
   Public g_bLoaded As Boolean
   Public g_bProjected As Boolean
   Public g_bDebug As Boolean
   Public g_bIndexOnly As Boolean

   Public Sub Main()
      Dim sCmd As String
      Dim iCnt, iTmp As Integer
      Dim asCnty(58) As String

      If InitSettings() = False Then
         MsgBox("Missing or bad INI file.  Please verify!")
         Exit Sub
      End If

      g_bStop = True
      g_bLoaded = False
      g_bDebug = False

      ' Parse command line
      iCnt = 0
      For Each argument As String In My.Application.CommandLineArgs
         If argument = "-R" Then
            g_bReplaceOnly = True
         ElseIf argument = "-I" Then
            g_bIndexOnly = True
         Else
            asCnty(iCnt) = argument
            iCnt += 1
         End If
      Next

      If iCnt > 0 Then
         For iTmp = 0 To iCnt - 1
            g_sCounty = asCnty(iTmp)
            If initCounty(g_sCounty) = False Then
               LogMsg("***** Cannot initialize county: " & g_sCounty & ".  Please check config file.")
            Else
               If g_bIndexOnly Then
                  Call doGeoInit()
               Else
                  Call doGeocode()
               End If
            End If

         Next
      Else
         'This needs rewrite.  Program will end when exiting this function
         'frmLocator.Show()
         Dim frmLocate As New frmLocator
         frmLocate.ShowDialog()

      End If

   End Sub

   Private Function InitSettings() As Boolean
      Dim sTmp As String
      Dim iTmp As Integer
      Dim bRet As Boolean

      On Error Resume Next

      InitSettings = True
      'g_sLogPath = GetIniData(My.Application.Info.AssemblyName, "System", "LogPath")
      g_sLogPath = My.Settings.LogPath
      If g_sLogPath = "" Then
         g_sLogPath = My.Application.Info.DirectoryPath
      End If

      'Set default log file
      g_logFile = g_sLogPath & "\EsriGC_" & Format(Now, "yyyyMMdd") & ".log"

      'Input record size
      'g_iRecSize = GetIniDataInt(My.Application.Info.AssemblyName, "System", "RecSize")
      g_iRecSize = My.Settings.RecSize

      'Start position of Geo data in the record
      'g_iGeoPos = GetIniDataInt(My.Application.Info.AssemblyName, "System", "GeoPos")
      g_iGeoPos = My.Settings.GeoPos

      'Replace data only - take from command line
      'g_bReplaceOnly = CBool(GetIniData(My.Application.Info.AssemblyName, "System", "ReplaceOnly"))

      'Setup src & dst template
      'g_srcTmpl = GetIniData(My.Application.Info.AssemblyName, "System", "SrcTmpl")
      'g_dstTmpl = GetIniData(My.Application.Info.AssemblyName, "System", "DstTmpl")
      g_srcTmpl = My.Settings.SrcTmpl
      g_dstTmpl = My.Settings.DstTmpl

      'Load county info from CountyInfo.csv
      'sTmp = GetIniData(My.Application.Info.AssemblyName, "System", "CntyInfo")
      'Call initCntyInfo(sTmp)
      Call initCntyInfo(My.Settings.CntyInfo)
      If g_iCounties < 58 Then
         MsgBox("Warning: Only " & g_iCounties & " counties are loaded.  Please verify!")
         InitSettings = False
         Exit Function
      End If

      'Default county
      'g_sCounty = GetIniData(My.Application.Info.AssemblyName, "System", "County")
      'bRet = initCounty(g_sCounty)

      'If bRet = False Then
      '   MsgBox("Cannot initialize county: " & g_sCounty)
      '   InitSettings = False
      'ElseIf Err.Number Then
      '   MsgBox(Err.Description)
      '   InitSettings = False
      'Else
      '   InitSettings = True
      'End If
      InitSettings = True

   End Function

   'Initialize default filenames for county
   Public Function initCounty(ByRef sCounty As String) As Boolean
      Dim sTmp As String
      Dim bRet As Boolean

      On Error GoTo InitCounty_Error

      bRet = False

      'Default county
      If sCounty <> "" Then
         'Change log file
         g_logFile = g_sLogPath & "\EGeo_" & sCounty & ".log"

         'Setup file I/O
         g_srcFile = Replace(g_srcTmpl, "???", sCounty)
         g_dstFile = Replace(g_dstTmpl, "???", sCounty)

         'Setup GIS database
         'g_sGISBase = GetIniData(My.Application.Info.AssemblyName, sCounty, "GISBase")
         'g_sGISTable = GetIniData(My.Application.Info.AssemblyName, sCounty, "GISTable")
         'g_sGISIndex = GetIniData(My.Application.Info.AssemblyName, sCounty, "GISIndex")
         For Each sTmp In My.Settings(sCounty)
            If Left(sTmp, 7) = "GISBase" Then
               g_sGISBase = Mid(sTmp, 9)
            ElseIf Left(sTmp, 8) = "GISTable" Then
               g_sGISTable = Mid(sTmp, 10)
            ElseIf Left(sTmp, 8) = "GISIndex" Then
               g_sGISIndex = Mid(sTmp, 10)
            ElseIf Left(sTmp, 5) = "GeoCS" Then
               g_iGeoCS = Mid(sTmp, 7)
            ElseIf Left(sTmp, 5) = "PrjCS" Then
               g_iPrjCS = Mid(sTmp, 7)
            ElseIf Left(sTmp, 9) = "FormatApn" Then
               g_iApnFormat = Mid(sTmp, 11)
            ElseIf Left(sTmp, 6) = "UseGis" Then
               g_iUseGis = Mid(sTmp, 8)
            ElseIf Left(sTmp, 6) = "ApnLen" Then
               g_iApnLen = Mid(sTmp, 8)
            ElseIf Left(sTmp, 9) = "Projected" Then
               sTmp = Mid(sTmp, 11)
               If UCase(sTmp) = "Y" Then
                  g_bProjected = True
               Else
                  g_bProjected = False
               End If
            ElseIf Left(sTmp, 5) = "Debug" Then
               sTmp = Mid(sTmp, 7)
               If sTmp = "Y" Then
                  g_bDebug = True
               Else
                  g_bDebug = False
               End If
            End If
         Next

         'Get Geographic Coordinate System
         'sTmp = GetIniData(My.Application.Info.AssemblyName, sCounty, "GeoCS")
         'g_iGeoCS = CInt(sTmp)
         'Get Projection Coordinate System
         'sTmp = GetIniData(My.Application.Info.AssemblyName, sCounty, "PrjCS")
         'g_iPrjCS = CInt(sTmp)

         'Format APN for searching?
         '0=no, 1=yes, 2=remove blank
         'g_iApnFormat = GetIniDataInt(My.Application.Info.AssemblyName, sCounty, "FormatApn")

         'Check UseGis flag: 1(situs), 2(mail), 4(Acreage)
         'g_iUseGis = GetIniDataInt(My.Application.Info.AssemblyName, sCounty, "UseGis")

         'Get projected flag
         'sTmp = GetIniData(My.Application.Info.AssemblyName, sCounty, "Projected")
         'If UCase(sTmp) = "Y" Then
         '   g_bProjected = True
         'Else
         '   g_bProjected = False
         'End If

         'Get Debug flag
         'sTmp = GetIniData(My.Application.Info.AssemblyName, sCounty, "Debug")
         'If sTmp = "Y" Then
         '   g_bDebug = True
         'Else
         '   g_bDebug = False
         'End If

         bRet = setCounty(sCounty)
      End If

InitCounty_Error:
      If Err.Number Then
         LogMsg(Err.Description)
         initCounty = False
      Else
         initCounty = bRet
      End If
   End Function
End Module