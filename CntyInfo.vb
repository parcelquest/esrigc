Option Strict Off
Option Explicit On
Module CntyInfo
	
	Structure CntyInfo
		Dim sCountyCode As String
      Dim iCountyID As Integer
      Dim iApnLen As Integer
      Dim iBookLen As Integer
      Dim iPageLen As Integer
      Dim iCmpLen As Integer
      Dim sCountyName As String
		Dim sApnFormat As String
		Dim sOtherFormat As String
		Dim sBook As String
	End Structure
	
	Public atCountyInfo(MAX_CNTYS) As CntyInfo
   Public g_iCounties As Integer
   Public g_iCntyIdx As Integer
   Public g_aApnParts() As Integer
   Public g_iApnParts As Integer
	
	Public Sub initCntyInfo(ByRef sFilename As String)
		Dim aStr() As String
      Dim fh, iRet As Integer
		Dim strTmp As String
		
		On Error GoTo initCntyInfo_Error
		
		fh = FreeFile
		FileOpen(fh, sFilename, OpenMode.Input)
		strTmp = LineInput(fh) 'Skip header line
		
		g_iCounties = 0
		Do While Not EOF(fh)
			strTmp = LineInput(fh)
			'CountyCode , CountyID, ApnLen, BookLen, PageLen, CmpLen, CountyName, ApnFormat, OtherFormat, Book
         g_iCounties = g_iCounties + 1

         'iRet = ParseStr(strTmp, ",", aStr)
         aStr = Split(strTmp, ",")
         If aStr.Length < 8 Then Exit Do
			atCountyInfo(g_iCounties).sCountyCode = aStr(0)
         atCountyInfo(g_iCounties).iCountyID = CInt(aStr(1))
         atCountyInfo(g_iCounties).iApnLen = CInt(aStr(2))
         atCountyInfo(g_iCounties).iBookLen = CInt(aStr(3))
         atCountyInfo(g_iCounties).iPageLen = CInt(aStr(4))
         atCountyInfo(g_iCounties).iCmpLen = CInt(aStr(5))
			atCountyInfo(g_iCounties).sCountyName = aStr(6)
			atCountyInfo(g_iCounties).sApnFormat = aStr(7)
			'atCountyInfo(g_iCounties).sOtherFormat = aStr(8)
			'atCountyInfo(g_iCounties).sBook = "" & aStr(9)
		Loop 
		FileClose(fh)
initCntyInfo_Error: 
	End Sub
	
   Public Function getCountyIdx(ByRef sCnty As String) As Integer
      Dim iRet As Integer

      For iRet = 1 To g_iCounties
         If atCountyInfo(iRet).sCountyCode = sCnty Then
            Exit For
         End If
      Next
      If iRet > g_iCounties Then
         iRet = 0
      End If
      getCountyIdx = iRet
   End Function
	
	Public Function setCounty(ByRef sCnty As String) As Boolean
      Dim iRet As Integer
		Dim aStr() As String
		
		If g_iCounties = 0 Then
			setCounty = False
			Exit Function
		End If
		
		iRet = getCountyIdx(sCnty)
		If iRet > 0 Then
			g_iCntyIdx = iRet
			'Get overide APN length
         If g_iApnLen = 0 Then
            g_iApnLen = atCountyInfo(g_iCntyIdx).iApnLen
         End If
			
			'Parse APN format
         'iRet = ParseStr(atCountyInfo(iRet).sApnFormat, "-", aStr)
         aStr = Split(atCountyInfo(iRet).sApnFormat, "-")
         g_iApnParts = aStr.Length
			
			ReDim g_aApnParts(g_iApnParts)
         For iRet = 0 To g_iApnParts - 1
            g_aApnParts(iRet) = Len(aStr(iRet))
         Next
			setCounty = True
		Else
			setCounty = False
		End If
		
	End Function
	
	Public Function formatApn(ByRef sApn As String) As String
      Dim iRet, iPos As Integer
		Dim sRet As String
		
		'This function only works after setCounty() is called
		If g_iCntyIdx = 0 Then
			formatApn = ""
			Exit Function
		End If
		
		'Standard formating
		sRet = Left(sApn, g_aApnParts(0))
		iPos = 1 + g_aApnParts(0)
		For iRet = 1 To g_iApnParts - 1
			sRet = sRet & "-" & Mid(sApn, iPos, g_aApnParts(iRet))
			iPos = iPos + g_aApnParts(iRet)
			If iPos > g_iApnLen Then Exit For
		Next 
		
		formatApn = sRet
	End Function
End Module